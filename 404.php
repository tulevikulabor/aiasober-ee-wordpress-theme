<?php
get_header();
?>
<main class="page-content" role="main">
    <section class="section-text container section-overlap__ section-overlap-bg__">
        <div class="section-bar container d-none">
            <a href="javascript:void(0)" class="btn btn-section" onclick="backAway()"><?php _e('Back', 'aiasober'); ?></a>
        </div>
        <div class="container text-center section-overlap-bg-inner__">
            <h1><?php _e('Page not found', 'aiasober'); ?></h1>
            <p>
                <span class="error_400"><?php _e('404', 'aiasober'); ?></span>
                <a href="javascript:void(0)" class="btn btn-section mt-5" style="text-align: center" onclick="backAway()"><?php _e('Back', 'aiasober'); ?></a>
            </p>
        </div>
    </section>
</main>
<?php
get_footer();
?>