<?php
/* Template Name: FAQ */

get_header();
?>
<main class="page-content" role="main">
    <section class="section-text container section-overlap__ section-overlap-bg__">
        <div class="section-bar container d-none">
            <a href="javascript:void(0)" class="btn btn-section" onclick="backAway()"><?php _e('Back', 'aiasober'); ?></a>
        </div>
        <div class="container">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
            
            <?php
            $faqs = cache()->remember('faqs-page-'.language(), FAQS_PAGE_CACHE_TIME, function () {
                $args = [
                    'post_type' => 'faqs',
                    'status' => 'publish',
                    'posts_per_page' => 20,
                    'limit' => 100,
                    'orderby' => 'menu_order',
                    'order'  => 'ASC',
                    'suppress_filters' => false,
                ];
                
                return get_posts($args);
            });
            ?>
            <div class="faqs accordion" id="accordionFaq">
            <?php foreach ($faqs as $faq) { ?>
                    
                <div class="faq mb-4">
                    <div class="faq-header">
                        <h2 class="m-0 p-0">
                            <a class="btn btn-link btn-block text-left p-3 d-flex align-items-center" data-toggle="collapse" data-target="#collapseFaq<?php echo $faq->ID; ?>" aria-expanded="false" aria-controls="collapseFaq<?php echo $faq->ID; ?>">
                                <?php echo $faq->post_title; ?> <div class="ml-auto"><span class="expand-button green"><i></i><i></i></span></div>
                            </a>
                        </h2>
                    </div>

                    <div id="collapseFaq<?php echo $faq->ID; ?>" class="collapse" data-parent="#accordionFaq">
                        <div class="faq-body p-4">
                            <?php echo $faq->post_content; ?>
                        </div>
                    </div>
                </div>
                
            <?php } ?>
            </div>
            
        </div>
    </section>
</main>

<?php
get_footer();
?>