# Tulevikulabor | Aiasober.ee Wordpress Theme vol 1

## Design

### Sketch

Source: https://www.sketch.com/s/d4a6999a-4e51-44f3-91bc-d1b726cd6f3e

Source folder: /design/

**Structure:**

+ /dist
+  |___ ...
+ /src
+  |___ /css
+  |___ /fonts
+  |___ /img
+  |     |___ /icons
+  |___ index.html

### Icons

https://freeicons.io/profile/3277?page=2

## HTML

**Folder:** /html/

## Wordpress Theme

## WooCommerce

## Development environments

HTML (Bootstrap): https://html.aiasober.ee

Shop (Wordpress): https://dev.aiasober.ee

Products import (Laravel): https://import-dev.aiasober.ee

## NPM / Yarn package fix

Read: https://gist.github.com/welcoMattic/9f0991fa81a80096a3877ee42562c504

```sh
$ npm rebuild jpegtran-bin
```

OR

```sh
$ yarn add --force jpegtran-bin
```

OR

```sh
autoconf automake file build-base nasm musl libpng-dev zlib-dev
```

## PHP Cache

### Run cache test

```sh
$ php composer serve:test
```

Then run in web browser:

http://127.0.0.1:8000/

OR

http://127.0.0.1:8000/index.php

OR

http://127.0.0.1:8000/cache.php

## Wordpress Theme Development

https://developer.wordpress.org/themes/basics/organizing-theme-files/

https://developer.wordpress.org/themes/basics/template-files/

https://codex.wordpress.org/Theme_Development#Theme_Classes

https://codex.wordpress.org/Theme_Development#Plugin_API_Hooks

https://codex.wordpress.org/Plugin_API

### Useful functions

```php
get_theme_file_path();

echo get_parent_theme_file_uri( 'images/logo.png' );
//or
echo get_parent_theme_file_path( 'images/logo.png' );

echo get_theme_file_uri( 'images/logo.png' );

echo get_permalink($ID);

// Styles: function.php

wp_enqueue_style( 'style', get_stylesheet_uri() );
wp_enqueue_style( $handle, $src, $deps, $ver, $media );
wp_enqueue_style( 'slider', get_template_directory_uri() . '/css/slider.css',false,'1.1','all');

// Scripts: function.php

wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer);
wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array ( 'jquery' ), 1.1, true);

function add_theme_scripts() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'slider', get_template_directory_uri() . '/css/slider.css', array(), '1.1', 'all');
    wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array ( 'jquery' ), 1.1, true);
    if ( is_singular() &amp;amp;amp;&amp;amp;amp; comments_open() &amp;amp;amp;&amp;amp;amp; get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

// User login

if ( is_user_logged_in() ):
    echo 'Welcome, registered user!';
else:
    echo 'Welcome, visitor!';
endif;

// Is ... page

echo home_url();

is_home()
is_front_page()
is_page()
is_page( 'about-me' )
is_page( array( 42, 'about-me', 'About Me And Joe' ) )
is_page_template( ‘about.php’ ) // Is a Page Template being used?

has_nav_menu()

if ( is_front_page() ) :
    get_header( 'home' );
elseif ( is_page( 'About' ) ) :
    get_header( 'about' );
else:
    get_header();
endif;

the_title()

the_content()
// or
the_excerpt()

```

### Useful functions: Data sanitize

```php
echo esc_html( $title );
<img src="<?php echo esc_url( $great_user_picture_url ); ?>" />
<a href="#" onclick="<?php echo esc_js( $custom_js ); ?>">Click me</a>
<ul class="<?php echo esc_attr( $stored_class ); ?>"> </ul>
<textarea><?php echo esc_textarea( $text ); ?></textarea>
esc_html_e( 'Hello World', 'text_domain' );
```

### Useful functions: Allowed HTML

```php
$allowed_html = [
    'a'      => [
        'href'  => [],
        'title' => [],
    ],
    'br'     => [],
    'em'     => [],
    'strong' => [],
];
echo wp_kses( $custom_content, $allowed_html );
```

### Meta

```php
get_post_meta($post->ID,'field name');
```

### Useful functions: Not found

```php
<div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
 
            <header class="page-header">
                <h1 class="page-title"><?php _e( 'Not Found', 'twentythirteen' ); ?></h1>
            </header>
 
            <div class="page-wrapper">
                <div class="page-content">
                    <h2><?php _e( 'This is somewhat embarrassing, isn’t it?', 'twentythirteen' ); ?></h2>
                    <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentythirteen' ); ?></p>
 
                    <?php get_search_form(); ?>
                </div><!-- .page-content -->
            </div><!-- .page-wrapper -->
 
        </div><!-- #content -->
    </div><!-- #primary -->
```

### Useful functions: Template

```php
<?php
/* Template Name: Example Template */
?>
```

### Useful functions: Search.php

```html
<header class="page-header">
    <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyfifteen' ), get_search_query() ); ?></h1>
</header>
```

### Menu

```php
wp_nav_menu(array(
    'menu'              => "Header main menu", // (int|string|WP_Term) Desired menu. Accepts a menu ID, slug, name, or object.
    'menu_class'        => "", // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
    'menu_id'           => "", // (string) The ID that is applied to the ul element which forms the menu. Default is the menu slug, incremented.
    'container'         => "", // (string) Whether to wrap the ul, and what to wrap it with. Default 'div'.
    'container_class'   => "", // (string) Class that is applied to the container. Default 'menu-{menu slug}-container'.
    'container_id'      => "", // (string) The ID that is applied to the container.
    'fallback_cb'       => "", // (callable|bool) If the menu doesn't exists, a callback function will fire. Default is 'wp_page_menu'. Set to false for no fallback.
    'before'            => "", // (string) Text before the link markup.
    'after'             => "", // (string) Text after the link markup.
    'link_before'       => "", // (string) Text before the link text.
    'link_after'        => "", // (string) Text after the link text.
    'echo'              => true, // (bool) Whether to echo the menu or return it. Default true.
    'depth'             => "", // (int) How many levels of the hierarchy are to be included. 0 means all. Default 0.
    'walker'            => "", // (object) Instance of a custom walker class.
    'theme_location'    => "", // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
    'items_wrap'        => "", // (string) How the list items should be wrapped. Default is a ul with an id and class. Uses printf() format with numbered placeholders.
    'item_spacing'      => "", // (string) Whether to preserve whitespace within the menu's HTML. Accepts 'preserve' or 'discard'. Default 'preserve'.
) );


$menus = get_registered_nav_menus();
foreach ( $menus as $location => $description ) {
 
    echo $location . ': ' . $description . '<br />';
}
```

### WooCommerce

https://www.businessbloomer.com/woocommerce-easily-get-product-info-title-sku-desc-product-object/

https://github.com/woocommerce/woocommerce/wiki/wc_get_products-and-WC_Product_Query

https://code.tutsplus.com/tutorials/display-woocommerce-categories-subcategories-and-products-in-separate-lists--cms-25479

https://www.businessbloomer.com/woocommerce-get-cart-info-total-items-etc-from-cart-object/

# WPML

## Config

### Stock

```xml
<wpml-config>
    <custom-fields>
        <custom-field action="copy">_stock</custom-field>
    </custom-fields>
</wpml-config>
```

# HTACCESS

https://perishablepress.com/stop-wordpress-changing-htaccess/

https://perishablepress.com/stop-wordpress-htaccess/

```php
// Stop WordPress from modifying .htaccess permalink rules
add_filter('flush_rewrite_rules_hard','__return_false');
```

OR

```
Default .htaccess permissions   = 644 (rw- r-- r--)
Read-only .htaccess permissions = 444 (r-- r-- r--)
```
