<?php
get_header();
?>
<main class="page-content" role="main">
    <section class="section-text container section-overlap__ section-overlap-bg__">
        <div class="section-bar container d-none">
            <a href="javascript:void(0)" class="btn btn-section" onclick="backAway()"><?php _e('Back', 'aiasober'); ?></a>
        </div>
        <div class="container section-overlap-bg-inner__">
            <h1><?php the_title(); ?></h1>
            
            <?php if (have_posts()) : ?>
                
                <?php while (have_posts()) : the_post(); ?>
             
                        <?php the_content(); ?>
        
                <?php endwhile; ?>
        
            <?php else : ?>
                <div class="alert alert-info">
                    <strong><?php _e('Content not found', 'aiasober'); ?></strong>
                </div>
            <?php endif; ?>
            
        </div>
    </section>
</main>
RAIDO

<?php
get_footer();
?>