<?php
/**
 * Plugin Name:         Aiasõber Faq Plugin
 * Description:         Aiasõber Faq Plugin allows to manage faq info.
 * Version:             1.0
 * Requires at least:   5.5
 * Requires PHP:        7.3
 * Author:              Raido Orumets <raido.orumets@gmail.com>
 * Author URI:          https://orumets.ee
 * Text Domain:         aiasober
 * Domain Path:         /languages
 */

add_action('init', 'aiasober_custom_post_faq');

function aiasober_custom_post_faq()
{
    $labels = [
        'name' => __('FAQs', 'faqs'),
        'singular_name'  => __('FAQs', 'faqs'),
        'add_new' => __('Add New FAQ', 'faqs'),
        'add_new_item' => __('Add New FAQ', 'faqs'),
        'edit_item'  => __('Edit FAQ', 'faqs'),
        'new_item' => __('New FAQ', 'faqs'),
        'all_items' => __('All FAQs', 'faqs'),
        'view_item' => __('View FAQ', 'faqs'),
        'search_items' => __('Search FAQs', 'faqs'),
        'featured_image'  => __('Image', 'faqs'),
        'set_featured_image' => __('Add image', 'faqs'),
    ];
    
    $args = [
        'labels' => $labels,
        'description' => __('FAQs'),
        'public' => true,
        'menu_position' => 6,
        'supports' => [
            'title',
            'editor',
            'thumbnail',
            'custom-fields',
            'revisions',
        ],
        'menu_icon' => 'dashicons-list-view',
        'exclude_from_search' => false,
        'show_in_admin_bar' => true,
        'query_var' => true,
    ];

    register_post_type('faqs', $args);
}
