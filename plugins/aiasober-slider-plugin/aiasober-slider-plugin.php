<?php
/**
 * Plugin Name:         Aiasõber Slider Plugin
 * Description:         Aiasõber Slider Plugin allows to manage header slider.
 * Version:             1.0
 * Requires at least:   5.5
 * Requires PHP:        7.3
 * Author:              Raido Orumets <raido.orumets@gmail.com>
 * Author URI:          https://orumets.ee
 * Text Domain:         aiasober
 * Domain Path:         /languages
 */

add_action('init', 'aiasober_custom_post_slide');

function aiasober_custom_post_slide()
{
    $labels = [
        'name' => __('Slider', 'slider'),
        'singular_name'  => __('Slide', 'slider'),
        'add_new' => __('Add New slide', 'slider'),
        'add_new_item' => __('Add New slide', 'slider'),
        'edit_item'  => __('Edit slide', 'slider'),
        'new_item' => __('New slide', 'slider'),
        'all_items' => __('All slides', 'slider'),
        'view_item' => __('View slide', 'slider'),
        'search_items' => __('Search slide', 'slider'),
        'featured_image'  => __('Image', 'slider'),
        'set_featured_image' => __('Add image', 'slider'),
    ];
    
    $args = [
        'labels' => $labels,
        'description' => __('Header slider'),
        'public' => true,
        'menu_position' => 5,
        'supports' => [
            'title',
            'editor',
            'thumbnail',
            'custom-fields',
            'revisions',
        ],
        'menu_icon' => 'dashicons-cover-image',
        'exclude_from_search' => false,
        'show_in_admin_bar' => true,
        'query_var' => true,
    ];

    register_post_type('slide', $args);
}

// Add custom Slider ID field to 'Edit Page'
add_action('add_meta_boxes', 'aiasober_meta_box_add');

function aiasober_meta_box_add()
{
    add_meta_box('button-meta-box-id', __('Button'), 'aiasober_meta_box_content', 'slide', 'normal', 'high');
}

function aiasober_meta_box_field($title, $name, $value, $placeholder = '')
{
    ?>
    <p>
        <label for="<?php echo $name; ?>" style="width: 100%; display: block;"><?php echo $title; ?></label>
        <input type="text" name="<?php echo $name; ?>" id="<?php echo $name; ?>" value="<?php echo esc_attr($value); ?>" placeholder="<?php echo $placeholder; ?>" style="width: 100%; display: block;" />
    </p>
    <?php
}

function aiasober_meta_box_content($post)
{
    $values = get_post_custom($post->ID);
    
    wp_nonce_field('my_meta_box_nonce', 'meta_box_nonce');
    aiasober_meta_box_field(__('Button name'), 'button_name', isset($values['button_name'][0]) ? $values['button_name'][0] : '', __('Read more'));
    aiasober_meta_box_field(__('Button link'), 'button_link', isset($values['button_link'][0]) ? $values['button_link'][0] : '', url());
}

add_action('save_post', 'aiasober_meta_box_save');

function aiasober_meta_box_save($post_id)
{
    // Bail if we're doing an auto save
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    // if our nonce isn't there, or we can't verify it, bail
    if (!isset($_POST['meta_box_nonce']) || !wp_verify_nonce($_POST['meta_box_nonce'], 'my_meta_box_nonce')) {
        return;
    }
    // if our current user can't edit this post, bail
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }
    // now we can actually save the data
    /* $allowed = array(
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    ); */
    // Probably a good idea to make sure your data is set
    if (isset($_POST['button_name'])) {
        update_post_meta($post_id, 'button_name', $_POST['button_name']); //wp_kses($_POST['button_name'] ,$allowed));
    }
    
    if (isset($_POST['button_link'])) {
        update_post_meta($post_id, 'button_link', $_POST['button_link']); //wp_kses($_POST['button_link'], $allowed));
    }
}
