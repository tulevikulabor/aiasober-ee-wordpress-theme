<?php
/**
 * Plugin Name:         Aiasõber Species Plugin
 * Description:         Aiasõber Species Plugin allows to manage species sction.
 * Version:             1.0
 * Requires at least:   5.5
 * Requires PHP:        7.3
 * Author:              Raido Orumets <raido.orumets@gmail.com>
 * Author URI:          https://orumets.ee
 * Text Domain:         aiasober
 * Domain Path:         /languages
 */

add_action('init', 'register_cpt_product');

function register_cpt_product()
{
    $labels = [
        'name' => _x('Liigikirjeldused', 'species'),
        'singular_name' => _x('Liigikirjeldus', 'species'),
        'add_new' => _x('Lisa uus', 'species'),
        'add_new_item' => _x('Lisa uus liigikirjeldus', 'species'),
        'edit_item' => _x('Muuda liigikirjelduss', 'species'),
        'new_item' => _x('Lisa uus liigikirjeldus', 'species'),
        'view_item' => _x('Vaata liigikirjeldust', 'species'),
        'search_items' => _x('Otsi liigikirjeldust', 'species'),
        'not_found' => _x('Ei leidnud', 'species'),
        'not_found_in_trash' => _x('Ei leidnud liigikirjeldusi prügikastist', 'species'),
        'parent_item_colon' => _x('Liigikirjeldus (parent) :', 'species'),
        'menu_name' => _x('Liigikirjeldused', 'species'),
    ];

    $args = [
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'Species filterable by category',
        'supports' => [
            'title',
            'editor',
            'thumbnail',
            'trackbacks',
            'custom-fields',
            'revisions',
            'tags'
        ],
        'taxonomies' => [
            'alphabet',
            'post_tag'
        ],
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-admin-page',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'capability_type' => 'post',
    ];

    register_post_type('species', $args);
    
    flush_rewrite_rules();
}

// Taxonomies

function createMyTaxonomies()
{
    register_taxonomy(
        'alphabet',
        'alphabet',
        array(
            'labels' => array(
                'name' => 'Liigikirjelduse kategooriad',
                'add_new_item' => 'Lisa uus kategooria',
                'new_item_name' => "Lisa uus kategooria"
            ),
            'show_ui' => true,
            'show_tagcloud' => true,
            'hierarchical' => true,
        )
    );
}
add_action('init', 'createMyTaxonomies', 0);

// Custom taxonomy term shortcode

function list_terms_custom_taxonomy($atts)
{
    extract(shortcode_atts(array(
        'custom_taxonomy' => '',
    ), $atts));

    $args = array(
        'taxonomy' => $custom_taxonomy,
        'title_li' => ''
    );

    echo '<ul class="species-categories">';
    echo wp_list_categories($args);
    echo '</ul>';
}

add_shortcode('ct_terms', 'list_terms_custom_taxonomy');

add_filter('register_post_type_args', 'wpse247328_register_post_type_args', 10, 2);
function wpse247328_register_post_type_args($args, $post_type)
{
    if ('species' === $post_type) {
        $args['rewrite']['slug'] = 'liigikirjeldused';
    }

    return $args;
}
