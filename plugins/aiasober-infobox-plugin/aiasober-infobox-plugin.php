<?php
/**
 * Plugin Name:         Aiasõber Info boxes Plugin
 * Description:         Aiasõber Info boxes Plugin allows to manage info boxes.
 * Version:             1.0
 * Requires at least:   5.5
 * Requires PHP:        7.3
 * Author:              Raido Orumets <raido.orumets@gmail.com>
 * Author URI:          https://orumets.ee
 * Text Domain:         aiasober
 * Domain Path:         /languages
 */

add_action('init', 'aiasober_custom_post_infobox');

function aiasober_custom_post_infobox()
{
    $labels = [
        'name' => __('Info boxes', 'infobox'),
        'singular_name'  => __('Info box', 'infobox'),
        'add_new' => __('Add New info box', 'infobox'),
        'add_new_item' => __('Add New info box', 'infobox'),
        'edit_item'  => __('Edit info box', 'infobox'),
        'new_item' => __('New info box', 'infobox'),
        'all_items' => __('All info boxes', 'infobox'),
        'view_item' => __('View info box', 'infobox'),
        'search_items' => __('Search info boxes', 'infobox'),
        'featured_image'  => __('Image', 'infobox'),
        'set_featured_image' => __('Add image', 'infobox'),
    ];
    
    $args = [
        'labels' => $labels,
        'description' => __('Info boxes'),
        'public' => true,
        'menu_position' => 5,
        'supports' => [
            'title',
            'editor',
            'thumbnail',
            'custom-fields',
            'revisions',
        ],
        'menu_icon' => 'dashicons-images-alt',
        'exclude_from_search' => false,
        'show_in_admin_bar' => true,
        'query_var' => true,
    ];

    register_post_type('infobox', $args);
}

// Add custom Info boxes ID field to 'Edit Page'
add_action('add_meta_boxes', 'aiasober_meta_infobox_add');

function aiasober_meta_infobox_add()
{
    add_meta_box('button-meta-box-id', __('Button'), 'aiasober_meta_infobox_content', 'infobox', 'normal', 'high');
}

function aiasober_meta_infobox_field($title, $name, $value, $placeholder = '')
{
    ?>
    <p>
        <label for="<?php echo $name; ?>" style="width: 100%; display: block;"><?php echo $title; ?></label>
        <input type="text" name="<?php echo $name; ?>" id="<?php echo $name; ?>" value="<?php echo esc_attr($value); ?>" placeholder="<?php echo $placeholder; ?>" style="width: 100%; display: block;" />
    </p>
    <?php
}

function aiasober_meta_infobox_content($post)
{
    $values = get_post_custom($post->ID);
    
    wp_nonce_field('my_meta_infobox_nonce', 'meta_box_nonce');
    aiasober_meta_infobox_field(__('Button name'), 'button_name', isset($values['button_name'][0]) ? $values['button_name'][0] : '', __('Read more'));
    aiasober_meta_infobox_field(__('Button link'), 'button_link', isset($values['button_link'][0]) ? $values['button_link'][0] : '', url());
}

add_action('save_post', 'aiasober_meta_infobox_save');

function aiasober_meta_infobox_save($post_id)
{
    // Bail if we're doing an auto save
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    // if our nonce isn't there, or we can't verify it, bail
    if (!isset($_POST['meta_box_nonce']) || !wp_verify_nonce($_POST['meta_box_nonce'], 'my_meta_infobox_nonce')) {
        return;
    }
    // if our current user can't edit this post, bail
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }
    // now we can actually save the data
    /* $allowed = array(
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    ); */
    // Probably a good idea to make sure your data is set
    if (isset($_POST['button_name'])) {
        update_post_meta($post_id, 'button_name', $_POST['button_name']); //wp_kses($_POST['button_name'] ,$allowed));
    }
    
    if (isset($_POST['button_link'])) {
        update_post_meta($post_id, 'button_link', $_POST['button_link']); //wp_kses($_POST['button_link'], $allowed));
    }
}
