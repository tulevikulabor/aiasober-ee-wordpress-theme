<footer>
    <div class="footer-top container">
        <div class="row">
            <div class="footer-company col col-12 col-lg-6 col-md-5 pr-3 pr-lg-5">
                <h3 class="mt-4">Aiasõber OÜ</h3>
                <p class="pr-0 pr-lg-5"><?php echo get_field('contacts_about_us', 'options'); ?></p>
            </div>
            <div class="footer-menu col col-12 col-lg-3 col-md-3 col-sm-6 pt-4">
                <h3><?php _e('Useful links', 'aiasober'); //echo menus('footer_menu')->title;?></h3>
                <ul>
                <?php foreach (menu('footer_menu') as $menud_id => $menu) { ?>
                    <li><a href="<?php echo $menu->url; ?>"><?php echo $menu->title; ?></a></li>
                <?php } ?>
                </ul>
            </div>
            <div class="footer-contacts col col-12 col-lg-3 col-md-4 col-sm-6 pt-4">
                <h3><?php _e('Contact details', 'aiasober'); ?></h3>
                <?php echo get_field('footer_contacts', 'options'); ?>
                <?php /* <ul>
                    <li class="d-flex">
                        <div><img src="<?php echo asset('img/icons/email.svg'); ?>" alt="" /></div><a href="mailto:aiasober@aiasober.ee">aiasober@aiasober.ee</a>
                    </li>
                    <li class="d-flex">
                        <div><img src="<?php echo asset('img/icons/phone.svg'); ?>" alt="" /></div><a href="tel:+3725529369">(+372) 552 9369</a>
                    </li>
                    <li class="d-flex">
                        <div><img src="<?php echo asset('img/icons/phone.svg'); ?>" alt="" /></div><a href="tel:+3725510131">(+372) 551 0131</a>
                    </li>
                    <li class="d-flex">
                        <div><img src="<?php echo asset('img/icons/pin.svg'); ?>" alt="" /></div>Ändi tee 18, Tüki küla, Tartu linn, 61411 Tartu maakond
                    </li>
                    <li class="d-flex">
                        <div><img src="<?php echo asset('img/icons/clock.svg'); ?>" alt="" /></div>Kontor: E-R 9:00 - 16:00
                    </li>
                    <li class="d-flex">
                        <div><img src="<?php echo asset('img/icons/clock.svg'); ?>" alt="" /></div>Aiand: 20. aprill - 1. oktoober<br />E-R 9.00-18.00, L-P 9.00-15.00
                    </li>
                </ul> */ ?>
            </div>
        </div>
    </div>
    <div class="footer-bottom container-fluid">
        <div class="container pt-0 pb-0 d-flex flex-column flex-md-row align-items-center">
            <div class="social-media-menu text-center text-md-left">
                <ul class="nav pb-3 pb-md-0">
                    <li class="nav-item pl-3 pl-md-0 pr-3"><a class="nav-link p-0 m-0" href="https://www.facebook.com/Aiasober/"><img src="<?php echo asset('img/social/facebook.svg'); ?>" alt="Facebook" /></a></li>
                    <li class="nav-item pl-3 pl-md-0 pr-3"><a class="nav-link p-0 m-0" href="https://www.instagram.com/aiasober/"><img src="<?php echo asset('img/social/instagram.svg'); ?>" alt="Instagram" /></a></li>
                    <li class="nav-item pl-3 pl-md-0 pr-3"><a class="nav-link p-0 m-0" href="https://www.youtube.com/channel/UCS1XYRNdVPNxfsKt0yYV5hQ"><img src="<?php echo asset('img/social/youtube.svg'); ?>" alt="YouTube" /></a></li>
                    <li class="nav-item pl-3 pl-md-0 pr-3"><a class="nav-link p-0 m-0" href="mailto:aiasober@aiasober.ee"><img src="<?php echo asset('img/social/email.svg'); ?>" alt="E-mail" /></a></li>
                </ul>
            </div>
            <div class="copyright text-center text-md-right ml-0 ml-md-auto">Aiasõber OÜ <?php _e('All rights reserved', 'aiasober'); ?> © <?php echo do_shortcode('[year]'); ?><span class="d-none d-md-inline">|</span></div>
            <div class="tulevikulabor text-center text-md-right pt-1 p-md-0"><a href="https://tulevikulabor.ee/" target="_blank"><img src="<?php echo asset('img/tulevikulabor.png'); ?>" alt="Tuleviku Labor - Rahvusvaheline loov digiagentuur - www.tulevikulabor.ee" data-toggle="tooltip" data-placement="bottom" title="Tuleviku Labor - Rahvusvaheline loov digiagentuur" /></a></div>
        </div>
    </div>
    
    <div itemscope itemtype="http://schema.org/Person" class="d-none">
        <span itemprop="name">Lembit Kaarna</span>
        <span itemprop="company">Aiasõber OÜ</span>
        <span itemprop="tel">+3725529369</span>
        <a itemprop="email" href="mailto:aiasober@aiasober.ee">aiasober@aiasober.ee</a>
    </div>
    
    <?php if (!env('production')) { ?>
        <!-- Ribbon -->
        <div class="corner-ribbon top-left sticky shadow env-<?php echo env(); ?>"><?php echo ucwords(env()); ?></div>​
    <?php } ?>
    
</footer>

<!-- symbols-modal -->
<div class="main-menu-modal modal left fade" id="main-menu-modal" tabindex="-1" role="dialog" aria-labelledby="main-menu-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="main-menu-modal-label"><?php _e('Menu', 'aiasober'); ?></h5>
                <button type="button" class="hamburger green active ml-auto" data-dismiss="modal" aria-label="Close">
                    <i></i><i></i><i></i>
                </button>
            </div>

            <div class="modal-body">
                <ul class="main-menu nav d-block">
                    <?php foreach (menu('main_menu') as $menud_id => $menu) { ?>
                        <?php if (!$menu->children) { ?>
                            <li><a class="nav-link<?php echo ($menu->active) ? ' active' : ''; ?>" href="<?php echo $menu->url; ?>"><?php echo $menu->title; ?></a></li>
                        <?php } else { ?>
                            <li><a class="nav-link<?php echo ($menu->active) ? ' active' : ''; ?> collapsed" data-toggle="collapse" data-target="#main-menu-products-<?php echo $menu->ID; ?>" href="<?php echo $menu->url; ?>" role="button" aria-expanded="false" aria-controls="main-menu-products-<?php echo $menu->ID; ?>"><?php echo $menu->title; ?></a>
                                <ul class="main-menu-submenu collapse" id="main-menu-products-<?php echo $menu->ID; ?>">
                                <?php
                                foreach ($menu->children as $menud_id => $menu) {
                                    $image = asset('img/categories/' . $menu->classes[0] . '.png');
                                    if (!file_exists($image)) {
                                        $image = asset('img/categories/default.png');
                                    } ?>
                                    <li><img src="<?php echo $image; ?>" alt="<?php echo $menu->title; ?>" /><a href="<?php echo $menu->url; ?>"><?php echo $menu->title; ?></a></li>
                                <?php
                                } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>

        </div>
    </div>
</div>

<section class="symbols">
    <!-- btn-symbols-modal -->
    <button type="button" class="btn btn-symbols-modal btn-symbols-modal-<?php echo strtolower(language()); ?>" data-toggle="modal" data-target="#symbols-modal">
        <span aria-hidden="true"><img src="<?php echo asset('img/icons/book-white.svg'); ?>" alt="<?php _e('Conventional signs', 'aiasober'); ?>" /></span> <?php _e('Conventional signs', 'aiasober'); ?>
    </button>
    <!-- symbols-modal -->
    <div class="symbols-modal modal right fade" id="symbols-modal" tabindex="-1" role="dialog" aria-labelledby="symbols-modal-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="symbols-modal-label"><?php _e('Conventional signs', 'aiasober'); ?></h5>
                    <button type="button" class="hamburger white active" data-dismiss="modal" aria-label="Close">
                        <i></i><i></i><i></i>
                    </button>
                </div>

                <div class="modal-body">
                    <ul class="symbols-list">
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-paikseline-kasvukoht"></div>
                            </div><?php _e('For sunny site', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-poolvarjuline-kasvukoht"></div>
                            </div><?php _e('For semi-shade location', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-varjuline-kasvukoht"></div>
                            </div><?php _e('For shade area', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-kuivale-pinnasele"></div>
                            </div><?php _e('For dry soil', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-parasniiskele-pinnasele"></div>
                            </div><?php _e('For normal soil', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-niiskele-pinnasele"></div>
                            </div><?php _e('Damp soil and waterfront', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-sobib-loikelilleks"></div>
                            </div><?php _e('Useful cut flowers', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-lohnavad-oied-voi-lehestik"></div>
                            </div><?php _e('Aromatic', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-sobib-potitaimeks"></div>
                            </div><?php _e('Good for potplant', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-sobib-amplisse ampel"></div>
                            </div><?php _e('Good for hanging basket', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-patenteeritud-sort"></div>
                            </div><?php _e('Patented variety', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-lehtdekoratiivne"></div>
                            </div><?php _e('Leaf decorative', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-putukaid-meelitav"></div>
                            </div><?php _e('Attractive for insects', 'aiasober'); ?>
                        </li>
                        <li class="d-flex align-items-center">
                            <div>
                                <div class="symbol-icon symbol-icon-mittesoodav"></div>
                            </div><?php _e('Non edible', 'aiasober'); ?>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</section>

<?php /* <script src="<?php echo asset('vendor/jquery/jquery.slim.min.js'); ?>"></script> */ ?>
<script src="<?php echo asset('vendor/bootstrap/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?php echo asset('js/vendor.js'); ?>"></script>
<script src="<?php echo asset('js/app.js'); ?>"></script>

<?php wp_footer(); ?>

</body>

</html>