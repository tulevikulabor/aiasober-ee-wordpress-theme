<?php

require_once __DIR__ . "/config.php";
require_once __DIR__ . "/Support/path.php";
require_once __DIR__ . "/Support/helpers.php";
require_once __DIR__ . "/Support/menu.php";
require_once __DIR__ . "/Support/page.php";
require_once __DIR__ . "/Support/woocommerce.php";
require_once __DIR__ . "/../plugins/plugins.php";

// Composer: Vendor
require_once __DIR__ . "/../vendor/autoload.php";
