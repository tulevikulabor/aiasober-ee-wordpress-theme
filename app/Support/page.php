<?php

function get_custom_field($field, $page, $default_value)
{
    global $wpdb;

    $value = get_post_meta($page->ID, $field, true);

    if ($value) {
        if (isset($_GET['custom_field_update'])) {
            $wpdb->update(
                $wpdb->postmeta,
                array(
                    'meta_value' => $default_value
                ),
                array(
                    'post_id' => $page->ID,
                    'meta_key' => $field
                ),
                array(
                    '%s'
                ),
                array(
                    '%d',
                    '%s'
                )
            );

            return $default_value;
        } else {
            return $value;
        }
    } else {
        $wpdb->insert(
            $wpdb->postmeta,
            array(
                'post_id' => $page->ID,
                'meta_key' => $field,
                'meta_value' => $default_value,
            ),
            array(
                '%d',
                '%s',
                '%s'
            )
        );

        return $default_value;
    }
}

function get_page_by_template($template_name = false)
{
    $pages = cache()->remember('page_template_by_template_' . $template_name, PAGE_TEMPLATE_CACHE_TIME, function () {
        return get_pages([
            'meta_key' => '_wp_page_template',
            'meta_value' => str_replace('.php.php', '.php', $template_name.'.php'),
        ]);
    });

    return $pages[0];
}

function get_sub_pages($page)
{
    $sub_pages = cache()->remember('sub_pages_page' . $page->ID, SUB_PAGES_CACHE_TIME, function () {
        return get_pages([
            'sort_order' => 'asc',
            'sort_column' => 'menu_order',
            'hierarchical' => 0,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => $page->ID,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
    });

    return $sub_pages;
}

function nl2p($string, $line_breaks = true, $xml = true)
{
    $string = str_replace(array('<p>', '</p>', '<br>', '<br />'), '', $string);

    // It is conceivable that people might still want single line-breaks
    // without breaking into a new paragraph.
    if ($line_breaks == true) {
        return '<p>'.preg_replace(array("/([\n]{2,})/i", "/([^>])\n([^<])/i"), array("</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'), trim($string)).'</p>';
    } else {
        return '<p>'.preg_replace(
            array("/([\n]{2,})/i", "/([\r\n]{3,})/i","/([^>])\n([^<])/i"),
            array("</p>\n<p>", "</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'),
            trim($string)
        ).'</p>';
    }
}
