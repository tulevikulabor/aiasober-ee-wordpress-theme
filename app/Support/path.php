<?php

// Functions

function __path($path)
{
    return str_replace('//', '/', $path);
}

function dd($variable)
{
    echo '<pre>';
    print_r($variable);
    echo '</pre>';
}

function array_to_object($array, &$obj)
{
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $obj->$key = new stdClass();
            array_to_object($value, $obj->$key);
        } else {
            $obj->$key = $value;
        }
    }
    return $obj;
}

// PATHs

function theme_path($path = null, $parent = false)
{
    if ($parent) {
        return get_theme_file_path(__path($path));
    } else {
        return get_parent_theme_file_path(__path($path));
    }
}

function cache_path($path = null)
{
    return theme_path($path ?? CACHE_PATH);
}

// URIs

function theme($path = null)
{
    return get_theme_file_uri(__path($path));
}

function assets($path = null)
{
    return theme(ASSETS_PATH . '/' . $path ?? ASSETS_PATH);
}

function asset($path = null)
{
    return assets($path);
}

function css($path = null)
{
    return theme(CSS_PATH . '/' . $path ?? CSS_PATH);
}

function js($path = null)
{
    return theme(JS_PATH . '/' . $path ?? JS_PATH);
}

function img($path = null)
{
    return theme(IMG_PATH . '/' . $path ?? IMG_PATH);
}

function vendor($path = null)
{
    return theme(VENDOR_PATH . '/' . $path ?? VENDOR_PATH);
}

// Cache

/*
$cache = cache()->remember('test', 10, function(){
    sleep(5);
    return 'Test';
});
 */

function cache($path = null)
{
    return new \App\Cache($path ?? cache_path());
}
