<?php

function url($id = null)
{
    if (is_integer($id)) {
        return get_permalink($id);
    } elseif (is_string($id)) {
        return site_url().__path('/'.$id);
    } else {
        return site_url();
    }
}

function menus($menu = null, $return_object = true)
{
    $menus = get_nav_menu_locations();
    $regisered_menus = get_registered_nav_menus();
    
    $array = [];
    
    foreach ($menus as $name => $id) {
        $array[$name] = [
            'ID' => $id,
            'name' => $name,
            'title' => __(isset($regisered_menus[$name]) ? $regisered_menus[$name] : $name),
        ];
    }

    $return = isset($array[$menu]) ? $array[$menu] : $array;
    
    return ($return_object) ? json_decode(json_encode($return)) : $return;
}

function menu($name_or_id, $return_object = true)
{
    if (is_string($name_or_id)) {
        $menu_id = menus($name_or_id)->ID;
    } else {
        $menu_id = $name_or_id;
    }
    
    $post = get_the_ID();
        
    $array_menu = wp_get_nav_menu_items($menu_id);
    $menu = array();
    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {
            $menu[$m->ID] = array();
            $menu[$m->ID]['ID'] = $m->ID;
            $menu[$m->ID]['active'] = ($m->object_id == $post) ? true : false;
            $menu[$m->ID]['name'] = $m->post_name;
            $menu[$m->ID]['title'] = $m->title;
            $menu[$m->ID]['guid'] = $m->url;
            $menu[$m->ID]['url'] = $m->url;
            $menu[$m->ID]['object_id'] = $m->object_id;
            $menu[$m->ID]['object'] = $m->object;
            $menu[$m->ID]['classes'] = $m->classes;
            $menu[$m->ID]['children'] = array();
        }
    }
    $submenu = array();
    foreach ($array_menu as $m) {
        if ($m->menu_item_parent) {
            $submenu[$m->ID] = array();
            $submenu[$m->ID]['ID'] = $m->ID;
            $submenu[$m->ID]['active'] = ($m->object_id == $post) ? true : false;
            $submenu[$m->ID]['name'] = $m->post_name;
            $submenu[$m->ID]['title'] = $m->title;
            $submenu[$m->ID]['guid'] = $m->url;
            $submenu[$m->ID]['url'] = $m->url;
            $submenu[$m->ID]['object_id'] = $m->object_id;
            $submenu[$m->ID]['object'] = $m->object;
            $submenu[$m->ID]['classes'] = $m->classes;
            $menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
        }
    }
    return ($return_object) ? json_decode(json_encode($menu)) : $menu;
}

function category($id_or_slug)
{
    if (is_string($id_or_slug)) {
        return get_category_by_slug($id_or_slugˇ);
    } else {
        return get_category($id_or_slug);
    }
}

function post($post_id)
{
    return get_post($post_id);
}

function locale()
{
    return get_locale();
}

function language()
{
    $locale = locale();
    list($language, $country) = explode("_", $locale);
    
    return $language;
}

function country()
{
    $locale = locale();
    list($language, $country) = explode("_", $locale);
    
    return strtolower($country);
}

function languages($is_country = false)
{
    if (function_exists('wpml_active_languages')) {
        
        /* $active_languages = cache()->remember('active_languages_'.language(), ACTIVE_LANGUAGES_CACHE_TIME, function () {
            return wpml_active_languages();
        }); */
        
        $active_languages = wpml_active_languages();
        
        return $active_languages;
    } else {
        $language = ($is_country) ? country() : language();
        
        $array = [];
        $array[$language] = [
            "id" => null,
            "active" => true,
            "native_name" => $language,
            "missing" => 0,
            "translated_name" => __($language),
            "language_code" => $language,
            "custom_flag_url" => asset('img/flags/' . $language . '.svg'),
            "country_flag_url" => asset('img/flags/' . $language . '.svg'),
            "url" => url(),
        ];
        
        return json_decode(json_encode($array));
    }
}

function language_menu()
{
    /* $language_menu = cache()->remember('language_menu_'.language(), LANGUAGE_MENU_CACHE_TIME, function () {
        return json_decode(json_encode(icl_get_languages('skip_missing=N&orderby=custom&order=asc&link_empty_to=str')));
    }); */
    
    $language_menu = json_decode(json_encode(icl_get_languages('skip_missing=N&orderby=custom&order=asc&link_empty_to=str')));
    
    return $language_menu;
}

function auth()
{
    $user = wp_get_current_user();
    //return (new \App\Auth($user))->__construct($user);
    return (new \App\Auth($user));
}

function image($id)
{
    $image = wp_get_attachment_url($id);
    $image = ($image) ? $image : url('wp-content/uploads/woocommerce-placeholder-600x600.png');
    
    return $image;
}
