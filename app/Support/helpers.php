<?php

function remove_thumbnail_dimensions($html)
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);

    return $html;
}

function environment($if_env = null)
{
    $host = $_SERVER['HTTP_HOST'];
    
    if (preg_match("/.dev|local.|localhost|127.0.0.1/i", $host)) {
        $env = 'local';
    } elseif (preg_match("/dev.|.dev|orumets|wordpress|arendus.|localhost|127.0.0.1/i", $host)) {
        $env = 'development';
    } elseif (preg_match("/test.|.test|prelive./i", $host)) {
        $env = 'staging';
    } else {
        $env = 'production';
    }
    
    if ($if_env) {
        return (strtolower($if_env) == $env) ? true : false;
    } else {
        return $env;
    }
}

function env($if_env = null)
{
    return environment($if_env);
}
