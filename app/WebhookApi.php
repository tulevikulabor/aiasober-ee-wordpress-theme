<?php

namespace App;

class WebhookApi
{
    private $request_token;
    private $key;
    
    public function __construct($request_token)
    {
        if (!$request_token) {
            http_response_code(400);
            exit();
        }
        $this->request_token = $request_token;
        $this->key = AIASOBER_THEME_API_KEY;
    }
    
    public function validate()
    {
        if ($this->request_token == $this->key) {
            http_response_code(200);
            return true;
        } else {
            http_response_code(403);
            return false;
        }
    }
}
