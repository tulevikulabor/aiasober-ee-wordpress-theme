<?php

namespace App;

class Auth
{
    public $user = false;
    
    public function __construct($user)
    {
        $this->user = $user; //($user->exists()) ? $user : false;
        
        return $this->user;
    }
    
    public function exists()
    {
        return $this->user->exists();
    }
    
    public function user()
    {
        return ($this->user->exists()) ? $this->user->data : false;
    }
    
    public function data()
    {
        return ($this->user->exists()) ? $this->user : false;
    }
}
