<?php

// System

define('DISALLOW_FILE_MODS', false);

// Assets

define('ASSETS_PATH', '/html/dist');
define('CSS_PATH', ASSETS_PATH . '/css');
define('JS_PATH', ASSETS_PATH . '/js');
define('IMG_PATH', ASSETS_PATH . '/img');
define('VENDOR_PATH', ASSETS_PATH . '/vendor');

// Cache

define('CACHE_PATH', '/cache');

define('LANGUAGE_MENU_CACHE_TIME', 60);
define('ACTIVE_LANGUAGES_CACHE_TIME', 60);
define('FRONT_PAGE_SLIDES_CACHE_TIME', 60);
define('FRONT_PAGE_INFOBOXES_CACHE_TIME', 60);
define('FRONT_PAGE_NEWS_CACHE_TIME', 60);
define('SUB_PAGES_CACHE_TIME', 60);
define('PAGE_TEMPLATE_CACHE_TIME', 60);
define('FAQS_PAGE_CACHE_TIME', 60);


// API key

define('AIASOBER_THEME_API_KEY', '3Mf6GHaRGQdmSYDlrt0fNiylZ9btmqIhMS2LKqDZ');