<?php

namespace App;

use Wruczek\PhpFileCache\PhpFileCache;

class Cache extends PhpFileCache
{
    public function remember($key, $cacheTime, $refreshCallback, $meta = false)
    {
        return $this->refreshIfExpired($key, $refreshCallback, $cacheTime ?? 60, $meta);
    }
    
    public function clear()
    {
        return exec('find ' . $this->getCacheDir() . ' -type f -name "*.cache.php" -exec rm -f {} \;');
    }
}
