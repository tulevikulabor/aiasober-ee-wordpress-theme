<?php

// Latin name in cart
add_filter('woocommerce_get_item_data', 'wc_add_cooking_to_cart', 10, 2);
function wc_add_cooking_to_cart($cart_data, $cart_item)
{
    $custom_items = array();

    if (!empty($cart_data)) {
        $custom_items = $cart_data;
    }

    // Get the product ID
    $product_id = $cart_item['product_id'];

    if ($custom_field_value = get_post_meta($product_id, 'latin_name', true)) {
        $custom_items[] = array(
            'name'      => __('latinname', 'woocommerce'),
            'value'     => $custom_field_value,
            'display'   => $custom_field_value,
        );
    }

    return $custom_items;
}

 add_action('woocommerce_before_checkout_form', 'finecode_cart_on_checkout_page_only', 5);
 
function finecode_cart_on_checkout_page_only()
{
    if (is_wc_endpoint_url('order-received')) {
        return;
    }
    echo do_shortcode('[woocommerce_cart]');
}

function fc_refresh_cart_count_ajax($fragments)
{
    ob_start();
    global $woocommerce; ?>
    <a class="btn btn-cart" href="javascript:void(0)" id="cart"><i class="fa fa-shopping-cart"></i> <span class="badge badge-success"><?php echo $woocommerce->cart->cart_contents_count; ?></span> - <?php
        echo $woocommerce->cart->get_cart_total(); ?></a>
    <?php
    $fragments['a#cart'] = ob_get_clean();
    return $fragments;
}
add_filter('woocommerce_add_to_cart_fragments', 'fc_refresh_cart_count_ajax');

/**
 * @snippet  Redirect Cart to Checkout - WooCommerce
 */
// add_filter ( 'woocommerce_add_to_cart_redirect', 'redirect_to_checkout' );
// function redirect_to_checkout() {
    
    // global $woocommerce;

    // Remove the default `Added to cart` message
    // wc_clear_notices();

    // return $woocommerce->cart->get_checkout_url();
    
// }

/* add_action( 'template_redirect', 'redirect_to_checkout_if_cart' );
function redirect_to_checkout_if_cart() {

    if ( !is_cart() ) return;

    global $woocommerce;

    if ( $woocommerce->cart->is_empty() ) {
        // If empty cart redirect to home
        wp_redirect( get_home_url(), 302 );
    } else {
        // Else redirect to check out url
        wp_redirect( $woocommerce->cart->get_checkout_url(), 302 );
    }

    exit;
} */
