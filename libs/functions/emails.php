<?php

/* emaili tekstid vastavalt valitud makseviisile */
function add_custom_text_to_email_setting($settings)
{
    $updated_settings = [];

    foreach ($settings as $section) {
        if (isset($section['id']) && 'email_template_options' == $section['id'] &&
       isset($section['type']) && 'sectionend' == $section['type']) {
            $updated_settings[] = [
                'name'     => __('Ettemaks', 'wc_invoice_custom_text'),
                'desc_tip' => __('Tekst, mida näeb klient, kes valis kassas makseviisiks ettemaksuga tasumise variandi', 'wc_invoice_custom_text'),
                'id'       => 'wc_invoice_custom_text',
                'type'     => 'textarea',
                'css'      => 'min-width:300px;',
                'std'      => '1',  // WC < 2.0
                'default'  => '',  // WC >= 2.0
                'desc'     => __('', 'wc_invoice_custom_text'),
            ];
      
            $updated_settings[] = [
                'name'     => __('Tasumine arvega', 'wc_invoice_bacs_text'),
                'desc_tip' => __('Tekst, mida näeb klient, kes valis kassas makseviisiks arvega tasumise variandi', 'wc_invoice_bacs_text'),
                'id'       => 'wc_invoice_bacs_text',
                'type'     => 'textarea',
                'css'      => 'min-width:300px;',
                'std'      => '1',  // WC < 2.0
                'default'  => '',  // WC >= 2.0
                'desc'     => __('', 'wc_invoice_bacs_text'),
            ];
      
            $updated_settings[] = [
                'name'     => __('Tasumine aiandis', 'wc_invoice_cod_text'),
                'desc_tip' => __('Tekst, mida näeb klient, kes valis kassas makseviisiks kohapeal tasumise variandi', 'wc_invoice_cod_text'),
                'id'       => 'wc_invoice_cod_text',
                'type'     => 'textarea',
                'css'      => 'min-width:300px;',
                'std'      => '1',  // WC < 2.0
                'default'  => '',  // WC >= 2.0
                'desc'     => __('', 'wc_invoice_cod_text'),
            ];
        }
        $updated_settings[] = $section;
    }

    return $updated_settings;
}
add_filter('woocommerce_email_settings', 'add_custom_text_to_email_setting');
