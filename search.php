<?php
get_header();
global $wp_query;
?>
<main class="page-content" role="main">
    <section class="section-text container section-overlap__ section-overlap-bg__">

        <div class="container section-overlap-bg-inner__">
            <h1><?php echo $wp_query->found_posts; ?> <?php printf( esc_attr__( 'Search Results for: %s', 'storefront' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
            
            <?php if (have_posts()) : ?>
				
				<ul class="search-results row px-0 mx-0">
	                
	                <?php while (have_posts()) : the_post(); ?>
	             
	                    <?php //the_content();?>
						<li class="search-result col-12">
							<h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
							<img src="<?php the_post_thumbnail_url('medium'); ?>" class="search-result-image rounded pr-2" alt="" />
							<?php echo substr(get_the_excerpt(), 0, 200); ?>
							<div class="search-result-more">
								<a class="search-result-more-link" href="<?php the_permalink(); ?>"><?php _e('Read more', 'aiasober'); ?></a>
							</div>
						</li>
	        
	                <?php endwhile; ?>
					
				</ul>
				
				<div class="page-numbers-container mt-5">
					<div>
					<?php
					$big = 999999999; // need an unlikely integer
					echo paginate_links([
						'base' => str_replace($big, '%#%', get_pagenum_link($big)),
						'format' => '?paged=%#%',
						'current' => max(1, get_query_var('paged')),
						//'total' => $categories->max_num_pages,
						'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
						'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
					]);
					?>
					</div>
				</div>
        
            <?php else : ?>
                <div class="alert alert-info">
                    <strong><?php _e('Content not found', 'aiasober'); ?></strong>
                </div>
            <?php endif; ?>
			
        </div>
    </section>
</main>
<?php get_footer(); ?>