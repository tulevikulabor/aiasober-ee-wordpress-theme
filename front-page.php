<?php
get_header();
?>
<main class="first-page-content" role="main">
    <section class="container section-discount section-overlap section-overlap-bg">
        <div class="container section-overlap-bg-inner">
            <h1><?php _e('Sale products', 'aiasober'); ?></h1>
            <?php //echo do_shortcode('[adp_products_on_sale]'); ?>
            <div class="products row">
                <?php
                //$products = cache()->remember('front-page-product-discount-' . language(), 60, function () {
                    $args = [
                        'post_type' => 'product',
                        'status' => 'publish',
                        'posts_per_page' => 8,
                        'orderby' => 'rand',
                        'order' => 'rand',
                        'category' => array( 'hea-hind-2' ),
                        'meta_query' => [
                            'relation' => 'AND',
                            [
                                'key' => '_stock_status',
                                'value' => 'instock',
                                'compare' => '=',
                            ],
                            [
                                'relation' => 'OR',
                                [ // Simple products type
                                    'key'           => '_sale_price',
                                    'value'         => 0,
                                    'compare'       => '>',
                                    'type'          => 'numeric'
                                ],
                            ],
                            [
                                'relation' => 'OR',
                                [ // Variable products type
                                    'key'           => '_min_variation_sale_price',
                                    'value'         => 0,
                                    'compare'       => '>',
                                    'type'          => 'numeric'
                                ],
                            ],
                        ],
                    ];
                    
                    $products = wc_get_products($args);
                    // return $products;
                //});
                foreach ($products as $product) {
                    //print_r($product);
                    $meta = get_post_meta($product->get_id(), 'latin_name', true);
                    //printr($product->get_image());
                    $image = cache()->remember('image_product_'. $product->get_id(), 60, function () use ($product) {
                        return image($product->get_image_id());
                    });
                    ?>
                    <div class="product-wrapper col col-12 col-lg-3 col-md-4 col-sm-6">
                        <div class="product d-flex flex-column fill">
                            <div class="product-image"><a href="<?php echo url($product->get_id()); ?>"><img class="img-fluid" src="<?php echo $image; ?>" alt="<?php echo $tag->name; ?>" /></a></div>
                            <div class="product-symbols">
                            <?php foreach($product->get_tag_ids() as $tag_id) {
                                $tag = get_term($tag_id);
                                ?>
                                <div class="symbol-icon symbol-icon-<?php echo $tag->slug; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $tag->name; ?>"></div>
                            <?php } ?>
                            </div>
                            <div class="product-title"><a href="<?php echo url($product->get_id()); ?>"><?php echo $product->get_name(); ?></a></div>
                            <div class="product-meta"><?php echo $meta; ?></div>
                            <div class="product-price mt-auto">
                                <?php echo $product->get_price_html(); ?>
                                <?php /* <del>
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>
                                            <span class="woocommerce-Price-currencySymbol">&euro;</span><?php echo $product->get_sale_price(); ?>
                                        </bdi>
                                    </span>
                                </del>
                                <ins>
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>
                                            <span class="woocommerce-Price-currencySymbol">&euro;</span><?php echo $product->get_regular_price(); ?>
                                        </bdi>
                                    </span>
                                </ins> */ ?>
                            </div>
                            <div class="product-button"><a href="<?php echo url($product->get_id()); ?>" class="btn"><?php _e('Look closer', 'aiasober'); ?></a></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="products-button text-center">
                <a href="<?php echo get_category_link(130); ?>" class="btn btn-section"><?php _e('Browse products on sale', 'aiasober'); ?></a>
            </div>
        </div>
    </section>

    <section class="section-winter section-with-bg">
        <div class="container">
            <h2><?php _e('New products', 'aiasober'); ?></h2>
            <div class="products row">
                <?php
                
                // echo do_shortcode('[products category="new-arrivals"]');

                //$products = cache()->remember('front-page-product-new-' . language(), 60, function () {
                    $args = [
                        'post_type' => 'product',
                        'status' => 'publish',
                        
                        'posts_per_page' => 8,
                        'orderby' => 'rand',
                        'order' => 'rand',
                        'category' => array( 'uus-2021' ),
                        'meta_query' => [
                            [
                                'key' => '_stock_status',
                                'value' => 'instock',
                                'compare' => '=',
                            ],
                        ],
                    ];
                    
                    /*'meta_query' => [
                        [
                            'key' => '_stock_status',
                            'value' => 'instock',
                            'compare' => '=',
                        ]
                    ]*/
                    
                    $products = wc_get_products($args);
                    //return $products;
                //});
                foreach ($products as $product) {
                    $meta = get_post_meta($product->get_id(), 'latin_name', true);
                    //printr($product->get_image());
                    $image = cache()->remember('image_product_'. $product->get_id(), 60, function () use ($product) {
                        return image($product->get_image_id());
                    });
                    ?>
                    <div class="product-wrapper col col-12 col-lg-3 col-md-4 col-sm-6">
                        <div class="product d-flex flex-column fill">
                            <div class="product-image"><a href="<?php echo url($product->get_id()); ?>"><img class="img-fluid" src="<?php echo $image; ?>" alt="<?php echo $tag->name; ?>" /></a></div>
                            <div class="product-symbols">
                            <?php foreach($product->get_tag_ids() as $tag_id) {
                                $tag = get_term($tag_id);
                                ?>
                                <div class="symbol-icon symbol-icon-<?php echo $tag->slug; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $tag->name; ?>"></div>
                            <?php } ?>
                            </div>
                            <div class="product-title"><a href="<?php echo url($product->get_id()); ?>"><?php echo $product->get_name(); ?></a></div>
                            <div class="product-meta"><?php echo $meta; ?></div>
                            <div class="product-price mt-auto">
                                <?php echo $product->get_price_html(); ?>
                                <?php /* <del>
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>
                                            <span class="woocommerce-Price-currencySymbol">&euro;</span><?php echo $product->get_sale_price(); ?>
                                        </bdi>
                                    </span>
                                </del>
                                <ins>
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>
                                            <span class="woocommerce-Price-currencySymbol">&euro;</span><?php echo $product->get_regular_price(); ?>
                                        </bdi>
                                    </span>
                                </ins> */ ?>
                            </div>
                            <div class="product-button"><a href="<?php echo url($product->get_id()); ?>" class="btn"><?php _e('Look closer', 'aiasober'); ?></a></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="products-button text-center">
                <a href="<?php echo get_category_link(133); ?>" class="btn btn-section"><?php _e('Browse all new products', 'aiasober'); ?></a>
            </div>
        </div>
    </section>

    <section class="section-winter">
        <div class="container">
            <h2><?php echo __('Fresh news', 'aiasober'); ?></h2>
            <div class="news row">
            <?php
            //$products = cache()->remember('front_page_news_' . language(), FRONT_PAGE_NEWS_CACHE_TIME, function () {});
                
             $args = [
            'post_type'=> 'post',
            'orderby' => 'menu_order',
            'order'    => 'ASC',
            'paged' => 1,
            'posts_per_page' => 4,
            'post_status' => 'publish',
            'tax_query' => [
                [
                    'taxonomy' => 'category',
                    'field' => 'term_id',
                    'terms' => 36,
                    'post_type' => 'post',
                ],
            ],
            ];
            $result = new WP_Query( $args );
            ?>
            <?php if ( $result->have_posts() ) : ?>
            <?php while ( $result->have_posts() ) : $result->the_post(); ?>
                <?php
                $post_id = get_the_id();
                //$post_image = cache()->remember('image_post_'. $post_id, FRONT_PAGE_NEWS_CACHE_TIME, function () use ($post_id) {
                    
                    $image = get_the_post_thumbnail_url($post_id);
                    $image = ($image) ? $image : url('wp-content/uploads/woocommerce-placeholder-600x600.png');
                    
                    $post_image = $image;
                    
                    //return $image;
                //});
                $post_url = url($post_id);
                ?>
                <div class="news-item-wrapper col col-12 col-lg-3 col-md-4 col-sm-6">
                    <div class="news-item d-flex flex-column fill">
                        <div class="news-item-image">
                            <a href="<?php echo $post_url; ?>">
                                <div class="news-item-image-button news-item-image-button-hover"><?php _e('Read more', 'aiasober'); ?></div>
                                <img class="img-fluid" src="<?php echo $post_image; ?>" alt="<?php echo get_the_title(); ?>" />
                            </a>
                        </div>
                        <div class="news-item-title"><a href="<?php echo $post_url; ?>"><?php echo get_the_title(); ?></a></div>
                        <div class="news-item-date mt-auto"><?php echo date('d.m.Y', strtotime($post->post_modified)); ?></div>
                    </div>
                </div>
            <?php endwhile; ?>
            <?php endif; wp_reset_postdata(); ?>
            </div>
            <!--<div class="news-button text-center">
                <a href="#" class="btn btn-section"><?php _e('View blog', 'aiasober'); ?></a>
            </div>-->
    </section>

    <?php
    $default_infobox_images = [
        asset('img/about/about-1.svg'),
        asset('img/about/about-2.svg'),
        asset('img/about/about-3.svg'),
    ];
    $infoboxes = cache()->remember('front-page-infoboxes-'.language(), FRONT_PAGE_INFOBOXES_CACHE_TIME, function () {
        $args = [
            'post_type' => 'infobox',
            'post_status' => 'publish',
            'posts_per_page' => 3,
            'limit' => 3,
            'orderby' => 'menu_order',
            'order'  => 'ASC',
            'suppress_filters' => false,
        ];
        
        return get_posts($args);
    });
    ?>
    <section class="section-about container mb-5">
        <div class="row m-0">
        <?php
        $nr = 0;
        foreach ($infoboxes as $infobox) {
            
            $infobox_image = cache()->remember('image_infobox_'. $infobox->ID, FRONT_PAGE_INFOBOXES_CACHE_TIME, function () use ($infobox, $default_infobox_images, $nr) {
                
                $image = get_the_post_thumbnail_url($infobox->ID);
                $image = ($image) ? $image : $default_infobox_images[$nr];
                
                return $image;
            });
            
            $infobox_meta = cache()->remember('meta_infobox_'. $infobox->ID, FRONT_PAGE_INFOBOXES_CACHE_TIME, function () use ($infobox) {
                
                $meta = [
                    'button_name' => get_post_meta($infobox->ID, 'button_name', true),
                    'button_link' => get_post_meta($infobox->ID, 'button_link', true),
                ];
                return $meta;
            });
            ?>
            <div class="section-about-item<?php if ($nr == 1) { ?> section-about-item-border<?php } ?> rounded-left d-flex flex-column text-center px-4 col col-12 col-md-4">
                <div class="section-about-item-img">
                    <img src="<?php echo $infobox_image; ?>" alt="<?php echo $infobox->post_title; ?>" />
                </div>
                <div class="section-about-item-title"><?php echo $infobox->post_title; ?></div>
                <p class="section-about-item-text"><?php echo $infobox->post_content; ?></p>
                <div class="mt-auto">
                    <a href="<?php echo $infobox_meta['button_link']; ?>" class="btn btn-section"><?php echo $infobox_meta['button_name']; ?></a>
                </div>
            </div>
            <?php $nr++; } ?>
        </div>
    </section>

    <section class="section-text section-with-bg d-none">
        <div class="container">
            <h2><?php _e('About us', 'aiasober'); ?></h2>
            <p>Osaühing AIASÕBER on perefirma, mis on asutatud 28-ndal septembril 1990 algselt aktsiaseltsina EREL. Peamine algusaastate tegevusala oli aianduskaupade tootmine ja eksport. Oma praeguse nime on äriühing saanud ettevõtte
                ümberkujundamise käigus 1997. aasta juunis.</p>
        </div>
    </section>

</main>
<?php
get_footer();
?>