<?php
get_header();
?>
<main class="page-content" role="main">
    <section class="section-text container section-overlap__ section-overlap-bg__">

        <div class="container section-overlap-bg-inner__">
            <h1><?php the_title(); ?></h1>
            
            <?php if (is_shop() || is_product_category()) { ?>
            
            <div class="row py-0 my-0">
                <div class="section-left col-12 col-md-3">
                    <?php dynamic_sidebar('custom-woocommerce-products-sidebar'); ?>
                </div>
                <div class="section-right col-12 col-md-9">
            <?php } ?>
            
                <!-- Archive -->
                
                    <?php if (have_posts()) : ?>
                        
                        <?php while (have_posts()) : the_post(); ?>
                     
                                <?php the_content(); ?>
                
                        <?php endwhile; ?>
                
                    <?php else : ?>
                        <div class="alert alert-info">
                            <strong><?php _e('Content not found', 'aiasober'); ?></strong>
                        </div>
                    <?php endif; ?>
                    
                <!-- /Archive -->
                
            <?php if (is_shop() || is_product_category()) { ?>
                </div>
            </div>
            <?php } ?>
            
        </div>
    </section>
</main>
<?php
get_footer();
?>