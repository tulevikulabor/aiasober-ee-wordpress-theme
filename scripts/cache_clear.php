<?php

require_once __DIR__ . "/../vendor/autoload.php";

use App\Cache;

$cache = new Cache(__DIR__ . "/../cache/");
$cache->clear();

exit('OK');