<?php

require_once __DIR__ . "/../config.php";
require_once __DIR__ . "/../Support/path.php";
require_once __DIR__ . "/Support/helpers.php";

// Composer: Vendor
require_once __DIR__ . "/../../vendor/autoload.php";

$api = new WebhookApi();

if ($api->validate()) {
    exec(
        "cd ". __DIR__ . "/../../;" .
        "git pull;"
    );
}
