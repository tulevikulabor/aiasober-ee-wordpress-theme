<?php
get_header();

$category_id = get_queried_object()->term_id;
?>
<main class="page-content" role="main">
    <section class="section-text container section-overlap__ section-overlap-bg__">
        
        <div class="container section-overlap-bg-inner__">
			
			<h1 class="h1-spieces-alphabet"><?php echo single_cat_title();?></h1>
			
			<div class="row pt-3">
					
                <div id="species-filter-col" class="col-12 col-md-4 pb-4">
					
                    <div id="species-filter">
                        
    					<h2 class="mt-0 pt-0"><?php _e('Search for species', 'aiasober'); ?></h2>
                        
                        <div class="pr-3">
                            <?php echo do_shortcode('[wd_asp id=2]'); ?>
                        </div>
    					
    					<h2><?php _e('Filter by the alphabet', 'aiasober'); ?></h2>
    					
    					<?php
    					$categories = get_terms([
    						'taxonomy' => 'alphabet',
    						'parent'   => 0,
    						'orderby' => 'name',
    						'order'   => 'ASC',
    					]);
    					?>
    					<div class="spieces-categories pr-0 pr-md-3">
    						<ul class="row nav">
    						<?php foreach($categories as $category) { ?>
    							<li class="species-category col-3 col-md-4 col-lg-3 pb-3 d-flex justify-content-center"><a href="<?php echo get_category_link($category->term_id); ?>" class="species-category-link rounded"><?php echo $category->name; ?></a></li>
    						<?php } ?>
    						</ul>
    					</div>
    					
    					<?php dynamic_sidebar('sidebar'); ?>
                        
                    </div>
					
				</div>
				
				<div class="col-12 col-md-8">
					<?php
					$id = get_queried_object()->term_id;
					$args = [
						// 'posts_per_page' => 18,
						'post_type' => 'species',
						'paged' => $paged,
						'orderby' => 'post_title',
						'order'    => 'ASC',
						'tax_query' => [
							[
								'taxonomy' => 'alphabet',
								'field' => 'term_id',
								'terms' => $id,
								'post_type' => 'species',
							],
						],
					];
					$categories = new WP_Query($args);
					?>
					
					<?php if ( $categories->have_posts() ) : ?>
						<div class="species row">
							<?php while ( $categories->have_posts() ) : $categories->the_post(); ?>
								<div class="col-6 col-md-6 col-lg-4">
									
									<div class="species-item">
										<a href="<?php the_permalink(); ?>" class="species-item-link d-flex flex-column fill">
											<?php $url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large'); ?>
											<?php $url = $url[0]; ?>
                                            <div class="species-item-image">
                                                <img class="img-fluid" src="<?php echo $url;?>" alt="<?php the_title(); ?>" />
                                            </div>
											<h3 class="species-item-title"><?php the_title(); ?></h3>
										</a>
									</div>
									
								</div>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
					
					<div class="page-numbers-container mt-5">
						<div>
						<?php
						$big = 999999999; // need an unlikely integer
						echo paginate_links([
							'base' => str_replace($big, '%#%', get_pagenum_link($big)),
							'format' => '?paged=%#%',
							'current' => max(1, get_query_var('paged')),
							'total' => $categories->max_num_pages,
							'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
							'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
						]);
						?>
						</div>
					</div>
					
					<?php wp_reset_query(); ?>
					
				</div>
				
			</div>

		</div>
	</section>
</main>
<?php get_footer(); ?>