<div class="header-search-bar ml-auto ml-md-0">
    <form id="header-search-bar-form" class="form-inline d-none d-md-flex" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <input id="s" name="s" type="text" class="form-control header-search-bar-input" value="<?php echo get_search_query(); ?>" placeholder="<?php _e('Search products from our assortment...', 'aiasober'); ?>">
        
        <input type="hidden" name="post_type[]" value="product" />
        <!--<input type="hidden" name="post_type[]" value="species" />
        <input type="hidden" name="post_type[]" value="page" />
        <input type="hidden" name="post_type[]" value="faq" />
        <input type="hidden" name="post_type[]" value="post" />-->

        <button id="header-search-bar-submit" type="submit" class="btn header-search-bar-btn"></button>
    </form>
    <a class="btn header-search-bar-btn circle d-md-none" data-toggle="collapse" href="#header-mobile-search-bar" role="button" aria-expanded="false" aria-controls="header-mobile-search-bar"></a>
</div>