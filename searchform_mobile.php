<div id="header-mobile-search-bar" class="header-search-bar header-mobile-search-bar d-md-none w-100 collapse">
    <div class="container">
        <form id="header-mobile-search-bar-form" class="form-inline w-100 pb-3" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <input id="s" name="s" type="text" class="form-control header-search-bar-input flex-grow-1" value="<?php echo get_search_query(); ?>" placeholder="<?php _e('Search products from our assortment...', 'aiasober'); ?>">
            
            <input type="hidden" name="post_type[]" value="product" />
            <!--<input type="hidden" name="post_type[]" value="species" />
            <input type="hidden" name="post_type[]" value="page" />
            <input type="hidden" name="post_type[]" value="faq" />
            <input type="hidden" name="post_type[]" value="post" />-->

            <button id="header-mobile-search-bar-submit" type="submit" class="btn header-search-bar-btn"></button>
        </form>
    </div>
</div>