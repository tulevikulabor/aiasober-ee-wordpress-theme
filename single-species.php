<?php
get_header();

$category_id = get_queried_object()->term_id;

$post_id = get_the_ID();
$post = get_post($post_id);
$image_url = get_the_post_thumbnail_url($post_id);
?>
<main class="page-content" role="main">
    <section class="section-text container section-overlap__ section-overlap-bg__">
        
        <div class="container section-overlap-bg-inner__">
			
			<h1><?php the_title();?></h1>
			
			<div class="row pt-3">
				
                <div id="species-filter-col" class="col-12 col-md-4 pb-4">
                    
                    <div id="species-filter">
                        
                        <h2 class="mt-0 pt-0"><?php _e('Search for species', 'aiasober'); ?></h2>
                        
                        <div class="pr-3">
                            <?php echo do_shortcode('[wd_asp id=2]'); ?>
                        </div>
                        
                        <h2><?php _e('Filter by the alphabet', 'aiasober'); ?></h2>
                        
                        <?php
                        $categories = get_terms([
                            'taxonomy' => 'alphabet',
                            'parent'   => 0,
                            'orderby' => 'name',
                            'order'   => 'ASC',
                        ]);
                        ?>
                        <div class="spieces-categories pr-0 pr-md-3">
                            <ul class="row nav">
                            <?php foreach($categories as $category) { ?>
                                <li class="species-category col-3 col-md-4 col-lg-3 pb-3 d-flex justify-content-center"><a href="<?php echo get_category_link($category->term_id); ?>" class="species-category-link rounded"><?php echo $category->name; ?></a></li>
                            <?php } ?>
                            </ul>
                        </div>
                        
                        <?php dynamic_sidebar('sidebar'); ?>
                        
                    </div>
                    
                </div>
				
				<div class="col-12 col-md-8">
					<img class="single-spiece-img img-responsive rounded" src="<?php echo $image_url ?>" alt="<?php echo get_the_title(); ?>" />
					<?php echo apply_filters('the_content', $post->post_content); ?>
				</div>
				
			</div>
			
		</div>
	</section>
</main>
<?php get_footer(); ?>