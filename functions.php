<?php

define('DISALLOW_FILE_MODS', true);
define('DISALLOW_FILE_EDIT', true);

global $woocommerce;

require_once __DIR__ . '/app/includes.php';
require_once __DIR__ . '/libs/functions/cart.php';
require_once __DIR__ . '/libs/functions/emails.php';
//add_theme_support('woocommerce');

// add_filter( 'woocommerce_show_page_title', '__return_false' );
add_action('woocommerce_after_shop_loop_item_title', 'aiasober_display_latin_name_under_images', 10);
add_action('woocommerce_single_product_summary', 'aiasober_display_latin_name_under_images', 5);
//add_action('woocommerce_single_product_summary', 'aiasober_display_markused_ja_lisainfo_under_stock', 20);
//add_action('woocommerce_single_product_summary', 'aiasober_display_omadused_under_tags', 40);

function aiasober_display_latin_name_under_images()
{
    echo '<div class="meta-latin">' . get_field('latin_name') . '</div>';
}

function aiasober_display_markused_ja_lisainfo_under_stock()
{
    echo '<div class="meta-markused-ja-lisainfo">' . get_field('markused_ja_lisainfo') . '</div>';
}

function aiasober_display_omadused_under_tags()
{
    echo '<div class="meta-omadused pt-2">' . get_field('omadused') . '</div>';
    // Note: 'trade' is the slug of the ACF
}

add_filter('body_class', 'woo_shop_class');
// Add WooCommerce Shop Page CSS Class
function woo_shop_class($classes)
{
    if (is_shop()) {  // Set conditional
        $classes[] = 'woo-shop';
    } // Add Class
    return $classes;
}

// Menus
register_nav_menus(array(
    'main_menu' => __('Header main menu'),
    'main_menu_mobile' => __('Mobile main menu'),
    'footer_menu' => __('Kasulikud viited'),
    'social_media_menu' => __('Social media menu'),
    'login_menu' => __('Logi sisse'),
    'logout_menu' => __('Logi välja'),
));

function aiasober_register_sidebars()
{
    register_sidebar([
        'name' => 'Sidebar',
        'id' => 'sidebar',
        'name' => __('Sidebar'),
        'description' => __('Page sidebar'),
        'before_widget' => '<div class="sidebar">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ]);

    register_sidebar([
        'name' => 'Products sidebar',
        'id' => 'custom-woocommerce-products-sidebar',
        'before_widget' => '<div class="sidebar sidebar-products custom-woocommerce-products-sidebar">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ]);

    register_sidebar([
        'name' => 'Footer column 1',
        'id' => 'footer-column-1',
        'before_widget' => '<div class="footer-column-1">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ]);

    register_sidebar([
        'name' => 'Footer column 2',
        'id' => 'footer-column-2',
        'before_widget' => '<div class="footer-column-2">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ]);

    register_sidebar([
        'name' => 'Footer column 3',
        'id' => 'footer-column-3',
        'before_widget' => '<div class="footer-column-3">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ]);

    register_sidebar([
        'name' => 'Footer copyright',
        'id' => 'footer-copyright',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => '',
    ]);
}
add_action('widgets_init', 'aiasober_register_sidebars');

function aiasober_register_shortcode_date($atts)
{
    return date('Y');
}
add_shortcode('year', 'aiasober_register_shortcode_date');

// Allow Text widgets to execute shortcodes

add_filter('widget_text', 'do_shortcode');


function aiasober_enable_woo_commerce_sidebar()
{
    add_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
}
add_action('init', 'aiasober_enable_woo_commerce_sidebar');

// Image sizes

function remove_default_image_sizes($sizes)
{
    unset($sizes['medium_large']);
    unset($sizes['1536×1536']);
    unset($sizes['2048×2048']);
    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'remove_default_image_sizes');

// Save billing state data

add_action('woocommerce_thankyou', 'save_billing_state_data');
function save_billing_state_data($order_id)
{
    if (! $order_id) {
        return;
    }

    $order = get_post_meta($order_id);
    $order_billing_state = $order['_billing_state'][0];
    $user_id =  get_current_user_id();
    if (!empty($user_id)) {
        update_user_meta($user_id, 'billing_state', $order_billing_state);
    }
}

// Bank transfer order status from pending to proccessing
add_action('woocommerce_thankyou_bacs', 'bacs_order_payment_processing_order_status');
function bacs_order_payment_processing_order_status($order_id)
{
    if (! $order_id) {
        return;
    }

    // Get an instance of the WC_Order object
    $order = wc_get_order($order_id);

    if (in_array($order->get_status(), array('on-hold', 'pending', 'completed'))) {
        $order->update_status('processing');
    } else {
        return;
    }
}
// Cheque payment order status from pending to proccessing
add_action('woocommerce_thankyou', 'cheque_payment_method_order_status_to_processing');
function cheque_payment_method_order_status_to_processing($order_id)
{
    if (! $order_id) {
        return;
    }

    $order = wc_get_order($order_id);
    $order->update_status('processing');

    if (get_post_meta($order->id, '_payment_method', true) == 'pay_by_invoice') {
        WC()->mailer()->get_emails()['WC_Email_Customer_Processing_Order']->trigger($order_id);
    }
}

add_filter('woocommerce_thankyou_custom', 'bacs_order_payment_processing_order_status');

add_filter('wpo_wcpdf_invoice_title', 'wpo_wcpdf_invoice_title', 10, 2);
function wpo_wcpdf_invoice_title($title, $document)
{
    $title = __('Order', 'woocommerce');
    return $title;
}

add_filter('wpo_wcpdf_filename', 'wpo_wcpdf_custom_filename', 10, 4);
function wpo_wcpdf_custom_filename($filename, $template_type, $order_ids, $context)
{
    $invoice_string = _n('invoice', 'invoices', count($order_ids), 'woocommerce-pdf-invoices-packing-slips');
    $new_prefix = _n('tellimus', 'tellimused', count($order_ids), 'woocommerce-pdf-invoices-packing-slips');
    $new_filename = str_replace($invoice_string, $new_prefix, $filename);

    return $new_filename;
}

add_filter('wpo_wcpdf_myaccount_button_text', 'wpo_wcpdf_myaccount_button_text', 10, 1);
function wpo_wcpdf_myaccount_button_text($button_text)
{
    return __('Download', 'woocommerce'); // your preferred button text
}

// States

add_filter('woocommerce_states', 'custom_woocommerce_states');

function custom_woocommerce_states($states)
{
    $states['EE'] = [
        //0 => '-- Vali maakond --',
        strtoupper('Harju maakond') => __('Harju maakond', 'aiasober'),
        strtoupper('Hiiu maakond') => __('Hiiu maakond', 'aiasober'),
        strtoupper('Ida-Viru maakond') => __('Ida-Viru maakond', 'aiasober'),
        strtoupper('Jõgeva maakond') => __('Jõgeva maakond', 'aiasober'),
        strtoupper('Järva maakond') => __('Järva maakond', 'aiasober'),
        strtoupper('Lääne maakond') => __('Lääne maakond', 'aiasober'),
        strtoupper('Lääne-Viru maakond') => __('Lääne-Viru maakond', 'aiasober'),
        strtoupper('Põlva maakond') => __('Põlva maakond', 'aiasober'),
        strtoupper('Pärnu maakond') => __('Pärnu maakond', 'aiasober'),
        strtoupper('Rapla maakond') => __('Rapla maakond', 'aiasober'),
        strtoupper('Saare maakond') => __('Saare maakond', 'aiasober'),
        strtoupper('Tartu maakond') => __('Tartu maakond', 'aiasober'),
        strtoupper('Valga maakond') => __('Valga maakond', 'aiasober'),
        strtoupper('Viljandi maakond') => __('Viljandi maakond', 'aiasober'),
        strtoupper('Võru maakond') => __('Võru maakond', 'aiasober'),
    ];

    $states['FI'] = [
         //0 => '-- Valitse lääni --',
         strtoupper('Lapland') => __('Lapland', 'aiasober'),
         strtoupper('North Ostrobothnia') => __('North Ostrobothnia', 'aiasober'),
         strtoupper('Kainuu') => __('Kainuu', 'aiasober'),
         strtoupper('North Karelia') => __('North Karelia', 'aiasober'),
         strtoupper('Northern Savonia') => __('Northern Savonia', 'aiasober'),
         strtoupper('Southern Savonia') => __('Southern Savonia', 'aiasober'),
         strtoupper('South Ostrobothnia') => __('South Ostrobothnia', 'aiasober'),
         strtoupper('Central Ostrobothnia') => __('Central Ostrobothnia', 'aiasober'),
         strtoupper('Ostrobothnia') => __('Ostrobothnia', 'aiasober'),
         strtoupper('Pirkanmaa') => __('Pirkanmaa', 'aiasober'),
         strtoupper('Central Finland') => __('Central Finland', 'aiasober'),
         strtoupper('Satakunta') => __('Satakunta', 'aiasober'),
         strtoupper('Southwest Finland') => __('Southwest Finland', 'aiasober'),
         strtoupper('South Karelia') => __('South Karelia', 'aiasober'),
         strtoupper('Päijät-Häme') => __('Päijät-Häme', 'aiasober'),
         strtoupper('Kanta-Häme') => __('Kanta-Häme', 'aiasober'),
         strtoupper('Uusimaa') => __('Uusimaa', 'aiasober'),
         strtoupper('Kymenlaakso') => __('Kymenlaakso', 'aiasober'),
         strtoupper('Aland') => __('Åland', 'aiasober'),
    ];

    return $states;
}

/**
* Change WooCommerce products per page
*/

add_filter('loop_shop_per_page', 'my_new_products_per_page', 9999);

function my_new_products_per_page($pr_per_page)
{
    $pr_per_page = 30;
    return $pr_per_page;
}

// Change the Number of Columns Displayed Per Page
add_filter('loop_shop_columns', 'my_new_shop_columns');

function my_new_shop_columns($column_num)
{
    $column_num = 3;
    return $column_num;
}

// Hide not in stock from search query

add_action('pre_get_posts', 'custom_hide_out_of_stock_in_search');

function custom_hide_out_of_stock_in_search($query)
{
    if (! $query->is_admin() && $query->is_search() && $query->is_main_query()) {
        $query->set('meta_key', '_stock_status');
        $query->set('meta_value', 'instock');

        //$query->set('_stock_status', 'instock');

        //$meta_query = $query->get('meta_query');

        /*$meta_query = [];

        $meta_query[] = [
            'key'       => 'latin_name',
            'value'     => $query->query['s'],
            'compare'   => 'LIKE',
        ];

        $query->set('meta_query', $meta_query);*/
    }
}

// Hide not in stock from shop catalogue

// add_filter('pre_option_woocommerce_hide_out_of_stock_items', 'custom_hide_out_of_stock_option');

function custom_hide_out_of_stock_option($option)
{
    return 'yes';
}

// Remove uncategorized from frontend

add_filter('woocommerce_product_categories_widget_args', 'custom_woocommerce_product_subcategories_args');

function custom_woocommerce_product_subcategories_args($args)
{
    //$args['exclude'] = get_option('default_product_cat');
    //$args['exclude'] = array('55','68'); // Insert the product category IDs you wish to exclude
    return $args;
}

/*
add_filter( 'woocommerce_billing_fields', 'woo_filter_state_billing', 10, 1 );
add_filter( 'woocommerce_shipping_fields', 'woo_filter_state_shipping', 10, 1 );

function woo_filter_state_billing( $address_fields ) {
      $address_fields['billing_state']['required'] = false;
      return $address_fields;
}

function woo_filter_state_shipping( $address_fields ) {
       $address_fields['shipping_state']['required'] = false;
       return $address_fields;
}
*/

/**
 *  Checks if a post is the original or not
 *
 * @param int $post_id The post ID to check. Leave on 0 if using within a loop
 * @param string $type The element type to check: post_post|post_page|post_custom Defaults to post_post
 *
 * @return array
 *  */
function wpml_is_original($post_id = 0, $type = 'post_post')
{
    global $post, $sitepress;

    $output = array();

    // use current post if post_id is not provided
    $p_ID = $post_id == 0 ? $post->ID : $post_id;

    $el_trid = $sitepress->get_element_trid($p_ID, $type);
    $el_translations = $sitepress->get_element_translations($el_trid, $type);

    if (!empty($el_translations)) {
        $is_original = false;
        foreach ($el_translations as $lang => $details) {
            if ($details->original == 1 && $details->element_id == $p_ID) {
                $is_original = true;
            }
            if ($details->original == 1) {
                $original_ID = $details->element_id;
            }
        }
        $output['is_original'] = $is_original;
        $output['original_ID'] = $original_ID;
    }
    return $output;
}

/*
//Use it in your theme like this:
// on single.php or within a loop
$wpml_is_original = wpml_is_original();

$is_original = $wpml_is_original['is_original'];
$original_id = $wpml_is_original['original_ID'];
*/


/**
* Ajax functions to retrieve cart
**/

add_action('wp_ajax_nopriv_load_woo_cart', 'load_woo_cart');
add_action('wp_ajax_load_woo_cart', 'load_woo_cart');

function load_woo_cart()
{
    echo get_template_part('ajax-cart');
    die();
}


// Make the search to index custom
/**
 * Extend WordPress search to include custom fields
 * http://adambalee.com
 *
 * Join posts and postmeta tables
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join($join)
{
    global $wpdb;
    if (/*! is_admin() &&*/ is_search()) {
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }
    return $join;
}
//add_filter('posts_join', 'cf_search_join');

/**
 * Modify the search query with posts_where
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where($where)
{
    global $pagenow, $wpdb;
    if (/*! is_admin() &&*/ is_search()) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)",
            $where
        );
    }
    return $where;
}
//add_filter('posts_where', 'cf_search_where');

/**
 * Prevent duplicates
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct($where)
{
    global $wpdb;
    if (/*! is_admin() &&*/ is_search()) {
        return "DISTINCT";
    }
    return $where;
}
//add_filter('posts_distinct', 'cf_search_distinct');


/**
 * Join postmeta in admin post search
 *
 * @return string SQL join
 */
function custom_admin_product_search_join($join)
{
    global $pagenow, $wpdb;
    if (is_admin() && $pagenow == 'edit.php' && ! empty($_GET['post_type']) && $_GET['post_type'] == 'product' && ! empty($_GET['s'])) {
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }
    return $join;
}
//add_filter('posts_join', 'custom_admin_product_search_join');

/**
 * Filtering the where clause in admin post search query
 *
 * @return string SQL WHERE
 */
function custom_admin_product_search_where($where)
{
    global $pagenow, $wpdb;
    if (is_admin() && $pagenow == 'edit.php' && ! empty($_GET['post_type']) && $_GET['post_type'] == 'product' && ! empty($_GET['s'])) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)",
            $where
        );
    }
    return $where;
}
//add_filter('posts_where', 'custom_admin_product_search_where');

function custom_admin_product_search_distinct($where)
{
    global $wpdb;
    if (is_admin() && $pagenow == 'edit.php' && ! empty($_GET['post_type']) && $_GET['post_type'] == 'product' && ! empty($_GET['s'])) {
        return "DISTINCT";
    }
    return $where;
}
//add_filter('posts_distinct', 'custom_admin_product_search_distinct');

/**
 * Alter the query vars to include products which have the meta we are searching for.
 *
 * @param array $query_vars The current query vars.
 *
 * @return array
 */
function m_request_query($query_vars)
{
    global $typenow;
    global $wpdb;
    global $pagenow;

    if (is_admin() && 'product' === $typenow && isset($_GET['s']) && 'edit.php' === $pagenow) {
        $search_term  = esc_sql(sanitize_text_field($_GET['s']));
        // Split the search term by comma.
        $search_terms = explode(',', $search_term);
        // If there are more terms make sure we also search for the whole thing, maybe it's not a list of terms.
        if (count($search_terms) > 1) {
            $search_terms[] = $search_term;
        }
        // Cleanup the array manually to avoid issues with quote escaping.
        array_walk($search_terms, 'trim');
        array_walk($search_terms, 'esc_sql');
        $meta_key               = '_sku';
        $post_types             = array( 'product', 'product_variation' );

        //$query                  = "SELECT DISTINCT posts.ID as product_id, posts.post_parent as parent_id FROM {$wpdb->posts} posts LEFT JOIN {$wpdb->postmeta} AS postmeta ON posts.ID = postmeta.post_id WHERE postmeta.meta_key = '{$meta_key}' AND postmeta.meta_value IN  ('" . implode("','", $search_terms) . "') AND posts.post_type IN ('" . implode("','", $post_types) . "') ORDER BY posts.post_parent ASC, posts.post_title ASC";

        $query                  = "SELECT
                                        DISTINCT posts.ID as product_id,
                                        posts.post_parent as parent_id
                                    FROM
                                        {$wpdb->posts} posts
                                    LEFT JOIN
                                        {$wpdb->postmeta} AS postmeta
                                    ON
                                        posts.ID = postmeta.post_id
                                    WHERE
                                        (
                                        	postmeta.meta_key = '{$meta_key}' AND
                                        	postmeta.meta_value IN ('" . implode("','", $search_terms) . "')
                                        ) OR (
                                        	postmeta.meta_key = 'latin_name' AND
                                        	postmeta.meta_value LIKE '%{$search_terms[0]}%'
                                        ) AND
                                        posts.post_type IN ('" . implode("','", $post_types) . "')
                                    ORDER BY
                                        posts.post_parent ASC,
                                        posts.post_title ASC";

        $search_results         = $wpdb->get_results($query);
        $product_ids            = wp_parse_id_list(array_merge(wp_list_pluck($search_results, 'product_id'), wp_list_pluck($search_results, 'parent_id')));
        $query_vars['post__in'] = array_merge($product_ids, $query_vars['post__in']);
    }

    return $query_vars;
}

add_filter('request', 'm_request_query', 20);

/**
 * Adds WooCommerce catalog sorting options using postmeta, such as custom fields
 * Tutorial: http://www.skyverge.com/blog/sort-woocommerce-products-custom-fields/
 * GIT: https://gist.github.com/bekarice/0df2b2d54d6ac8076f84
**/
function skyverge_add_postmeta_ordering_args($sort_args)
{
    $orderby_value = isset($_GET['orderby']) ? wc_clean($_GET['orderby']) : apply_filters('woocommerce_default_catalog_orderby', get_option('woocommerce_default_catalog_orderby'));
    switch ($orderby_value) {

        // Name your sortby key whatever you'd like; must correspond to the $sortby in the next function
        case 'latin_name':
            $sort_args['orderby']  = 'meta_value';
            // Sort by meta_value because we're using alphabetic sorting
            $sort_args['order']    = 'asc';
            $sort_args['meta_key'] = 'latin_name';
            // use the meta key you've set for your custom field, i.e., something like "location" or "_wholesale_price"
            break;

        /* case 'points_awarded':
            $sort_args['orderby'] = 'meta_value_num';
            // We use meta_value_num here because points are a number and we want to sort in numerical order
            $sort_args['order'] = 'desc';
            $sort_args['meta_key'] = 'points';
            break; */

    }

    return $sort_args;
}
add_filter('woocommerce_get_catalog_ordering_args', 'skyverge_add_postmeta_ordering_args');


// Add these new sorting arguments to the sortby options on the frontend
function skyverge_add_new_postmeta_orderby($sortby)
{

    // Adjust the text as desired
    // $sortby['location'] = __('Sort by location', 'woocommerce');
    // $sortby['points_awarded'] = __('Sort by points for purchase', 'woocommerce');
    $sortby['latin_name'] = __('Sort by latin name', 'woocommerce');

    return $sortby;
}
add_filter('woocommerce_default_catalog_orderby_options', 'skyverge_add_new_postmeta_orderby');
add_filter('woocommerce_catalog_orderby', 'skyverge_add_new_postmeta_orderby');

/**
 * Admin CSS
 */

add_action('admin_head', 'my_custom_wordpress_admin_css');

function my_custom_wordpress_admin_css()
{
    ?>
    <!-- Custom Admin CSS -->
    <link rel="stylesheet" href="<?php echo asset('css/admin.css');
    if (auth()->exists()) {
        echo '?'.date('YmdHis');
    } else {
        echo '?'.date('YmdHis');
    } ?>">
    <?php
}
