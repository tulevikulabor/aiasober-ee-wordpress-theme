<table style="width:900px;margin:0 auto;">
    <tr style="border-bottom:1px solid #eaeaea;">
        <td colspan="2">
            <br />
            <br />
            <img style="display:block;margin:0 auto;max-width:250px;" src="https://pood.aiasober.ee/wp-content/uploads/2018/11/logo.png" alt="Aiasõber logo">
        </td>
    </tr>
    <tr style="border-bottom:1px solid #eaeaea;">
        <td colspan="2">
            <br />
            <br />
            <?php
            echo sprintf(esc_html__('Hi %s,', 'woocommerce'), esc_html($order->get_billing_first_name()));?><br />
            <?php
            if (get_post_meta($order->id, '_payment_method', true) == 'pay_by_invoice') {
                _e(get_option('wc_invoice_custom_text', ''), 'aiasober');
            } elseif (get_post_meta($order->id, '_payment_method', true) == 'bacs') {
                _e(get_option('wc_invoice_bacs_text', ''), 'aiasober');
            } elseif (get_post_meta($order->id, '_payment_method', true) == 'cod') {
                _e(get_option('wc_invoice_cod_text', ''), 'aiasober');
            } else {
                echo '';
            }
            ?>
        </td>
    </tr>
    <tr style="border-bottom:1px solid #eaeaea;">
        <td>
            <br />
            <br />
            <b><?php echo __('Customer details', 'aiasober');?></b><br />
            <?php echo $order->billing_first_name;?>
            <?php echo $order->billing_last_name;?><br />
            <?php if ($order->billing_address_1):?>
            <?php echo $order->billing_address_1;?><br />
            <?php endif;?>
            <?php if ($order->billing_address_2):?>
            <?php echo $order->billing_address_2;?><br />
            <?php endif;?>
            <?php echo $order->billing_postcode;?>
            <?php echo $order->billing_city;?><br />
            <?php echo $order->billing_email;?><br />
            <?php echo $order->billing_phone;?><br />
        </td>
        <td style="border-right:1px solid #eaeaea;">
            <br />
            <br />
            <b>
                <?php echo __('Company data', 'aiasober');?></b><br />
            <?php echo wp_kses_post(get_option('woocommerce_email_footer_text'));?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <br />
            <?php
            do_action('woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email);
            do_action('woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email);
            ?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <?php if ($order->customer_note):?>
            <b>
                <?php echo __('Additional notes', 'aiasober');?>:</b> <i>
                <?php echo $order->customer_note;?></i>
            <?php endif;?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <br />
            <?php
            if ($additional_content) {
                echo esc_html(wp_strip_all_tags(wptexturize($additional_content)));
            }
            ?>
            <br />
            <br />
        </td>
    </tr>
</table>