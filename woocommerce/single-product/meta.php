<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

$wpml_is_original = wpml_is_original($product->ID, 'post_product');
$is_original_product = $wpml_is_original['is_original'];
$original_product_id = $wpml_is_original['original_ID'];
?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>

	<?php endif; ?>

	<?php echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php // echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
	
	
	<?php
	$estProduct = icl_object_id($product->get_id(), 'product', false, 'et');
	if (!empty($estProduct)) {
		?>
		<div class="product-symbols py-3 d-flex">
		<?php
		foreach(WC_get_product($estProduct)->get_tag_ids() as $tagId){
			$estId = icl_object_id($tagId, 'product_tag', true, 'et');
			//$image = get_field('icon', 'product_tag' . '_' . $estId);
			
			global $icl_adjust_id_url_filter_off;
			$orig_flag_value = $icl_adjust_id_url_filter_off;
			$icl_adjust_id_url_filter_off = true;
			$term_et = get_term($estId, 'product_tag'); $icl_adjust_id_url_filter_off = $orig_flag_value;
			$term = get_term($tagId, 'product_tag');
			?>
			<div class="symbol-icon symbol-icon-<?php echo $term_et->slug;?> mr-1" data-toggle="tooltip" title="<?php echo $term->name;?>"></div>
			<?php
		}
		?>
		</div>
		<?php
	}
	?>
	
	<?php if ($is_original_product) { ?>
		
	<?php } else { ?>
		
	<?php } ?>
	
	<div class="meta-omadused pt-2">
		<?php //echo get_field('omadused', $original_product_id); ?>
		<ul class="omadused-list">
		<?php
		for ($i=1; $i<=4; $i++) {
			$name_label = "omadus_{$i}";
			$value_label = "vaartus_{$i}";
			$name = get_field($name_label, $original_product_id);
			$value = get_field($value_label, $original_product_id);
			$slug = sanitize_title($name);
			?>
			<li class="omadused-list-item omadused-key-<?php echo $slug; ?>">
				<span class="omadused-list-item-key"><?php _e($name, 'WordPress'); ?>: </span>
				<span class="omadused-list-item-value"><?php echo $value; ?></span>
			</li>
		<?php } ?>
		</ul>
	</div>
	
	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
