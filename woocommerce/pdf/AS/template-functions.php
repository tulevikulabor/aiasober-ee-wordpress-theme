<?php
/**
 * Use this file for all your template filters and actions.
 * Requires WooCommerce PDF Invoices & Packing Slips 1.4.13 or higher
 */
if (! defined('ABSPATH')) {
    exit;
} // Exit if accessed directly


#add_action('wpo_wcpdf_after_shipping_address', 'billingCounty');
function billingCounty()
{
    echo $order->get_meta('billing_county');
}
