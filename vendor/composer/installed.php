<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'c4ab1e5654a89d749d671f3144eac8b58047f349',
        'name' => 'tulevikulabor/aiasober-ee-wordpress-theme',
        'dev' => true,
    ),
    'versions' => array(
        'tulevikulabor/aiasober-ee-wordpress-theme' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'c4ab1e5654a89d749d671f3144eac8b58047f349',
            'dev_requirement' => false,
        ),
        'wruczek/php-file-cache' => array(
            'pretty_version' => '0.0.5',
            'version' => '0.0.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../wruczek/php-file-cache',
            'aliases' => array(),
            'reference' => '83b0da10bc4cbf03fcc6f593abe7bfb663719902',
            'dev_requirement' => false,
        ),
    ),
);
