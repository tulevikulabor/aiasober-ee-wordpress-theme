const mix = require('laravel-mix');
const fs = require('fs');
const fg = require('fast-glob');

const ImageminPlugin = require('imagemin-webpack-plugin').default;
const ImageminMozjpeg = require('imagemin-mozjpeg');
const CopyPlugin = require('copy-webpack-plugin');

const COUNTRIES = ['gb', 'ee', 'ru', 'fi'];

const COPY_FONTS_FROM_NODE_MODULES = false;
const COPY_VENDOR_FROM_NODE_MODULES = false;

const COPY_IMAGES = false;
const COPY_FONTS = false;
const COPY_VENDOR = false;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 | Example: mix.js('src/app.js', 'dist/').sass('src/app.scss', 'dist/');
 |
 */

mix.options({
    processCssUrls: false
});

mix.setPublicPath('dist');

if (mix.inProduction()) {

    mix.version();
    mix.disableNotifications();

    if (COPY_IMAGES) {

        mix.webpackConfig({
            plugins: [
                new CopyPlugin({
                    patterns: [{
                        from: 'src/img', // 'src/img'
                        to: 'img' // 'dist/img'
                    }],
                }),
                new ImageminPlugin({
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    plugins: [
                        ImageminMozjpeg({
                            quality: 87,
                        })
                    ]
                }),
            ]
        });
    }

} else {
    if (COPY_IMAGES) {

        COUNTRIES.forEach(function (country) {
            fs.mkdirSync('src/img/flags', {
                recursive: true
            });
            // fs.copyFileSync('node_modules/flag-icon-css/flags/4x3/' + country + '.svg', 'src/img/flags/' + country + '.svg');
            mix.copy('node_modules/flag-icon-css/flags/4x3/' + country + '.svg', 'src/img/flags/' + country + '.svg');
        });

        mix.copyDirectory('src/img', 'dist/img');
    }
}

if (COPY_FONTS_FROM_NODE_MODULES) {
    mix.copy('node_modules/@vetixy/circular-std/dist/index.css', 'src/css/fonts.scss');
    mix.copyDirectory('node_modules/@vetixy/circular-std/fonts', 'src/fonts');
} else {
    if (COPY_FONTS) {
        mix.copyDirectory('src/fonts', 'dist/fonts');
    }
}

if (COPY_VENDOR_FROM_NODE_MODULES) {
    // Fixto
    mix.copy('node_modules/fixto/dist/fixto.min.js', 'src/vendor/fixto/fixto.min.js');
    mix.copy('node_modules/fixto/dist/fixto.js', 'src/vendor/fixto/fixto.js');
    // Bootstrap
    mix.copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'src/vendor/bootstrap/bootstrap.min.css');
    mix.copy('node_modules/bootstrap/dist/css/bootstrap.min.css.map', 'src/vendor/bootstrap/bootstrap.min.css.map');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.min.js', 'src/vendor/bootstrap/bootstrap.min.js');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.min.js.map', 'src/vendor/bootstrap/bootstrap.min.js.map');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.bundle.min.js', 'src/vendor/bootstrap/bootstrap.bundle.min.js');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.bundle.min.js.map', 'src/vendor/bootstrap/bootstrap.bundle.min.js.map');
    // jQuery
    mix.copy('node_modules/jquery/dist/jquery.min.js', 'src/vendor/jquery/jquery.min.js');
    mix.copy('node_modules/jquery/dist/jquery.min.map', 'src/vendor/jquery/jquery.min.map');
    mix.copy('node_modules/jquery/dist/jquery.slim.min.js', 'src/vendor/jquery/jquery.slim.min.js');
    mix.copy('node_modules/jquery/dist/jquery.slim.min.map', 'src/vendor/jquery/jquery.slim.min.map');

    mix.copy('node_modules/slick-carousel/slick/slick.min.js', 'src/vendor/slick/slick.min.js');
    mix.copy('node_modules/slick-carousel/slick/slick.css', 'src/vendor/slick/slick.css');
    mix.copy('node_modules/slick-carousel/slick/slick.scss', 'src/vendor/slick/slick.scss');
}

// HTML pages

const PAGES = fg.sync(['src/*.html'], {
    objectMode: true
});

PAGES.forEach(function (page) {
    mix.copy('src/' + page.name, 'dist/' + page.name);
});

// JS

mix.babel([
    'src/js/load_first.js',
    'src/js/go_back.js',
    'src/js/symbols_modal.js',
    'src/js/checkout.js',
    'src/js/main.js',
], 'dist/js/app.js');

if (COPY_VENDOR) {
    mix.babel([
        'src/vendor/fixto/fixto.js',
        'src/vendor/slick/slick.min.js',
    ], 'dist/js/vendor.js');

    mix.combine([
        'src/vendor/slick/slick.css',
    ], 'dist/css/vendor.css');

    mix.copy('src/vendor/jquery/jquery.slim.min.js', 'dist/vendor/jquery/jquery.slim.min.js');
    mix.copy('src/vendor/bootstrap/bootstrap.bundle.min.js', 'dist/vendor/bootstrap/bootstrap.bundle.min.js');
    mix.copy('src/vendor/bootstrap/bootstrap.bundle.min.js.map', 'dist/vendor/bootstrap/bootstrap.bundle.min.js.map');
    mix.copy('src/vendor/bootstrap/bootstrap.min.css', 'dist/vendor/bootstrap/bootstrap.min.css');
    mix.copy('src/vendor/bootstrap/bootstrap.min.css.map', 'dist/vendor/bootstrap/bootstrap.min.css.map');
}

// CSS

mix.sass('src/css/app.scss', 'dist/css/');
mix.sass('src/css/wordpress/admin.scss', 'dist/css/');
mix.sass('src/admin/css/app.scss', 'dist/admin/css/');

/*.webpackConfig({
    module: {
        rules: [{
            test: /(\.(png|jpe?g|gif|webp)$|^((?!font).)*\.svg$)/,
            loaders: {
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]?[hash]',
                    context: 'src',
                }
            },
        }]
    }
}); */

//mix.minify('dist/css/app.css');

// Full API
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.preact(src, output); <-- Identical to mix.js(), but registers Preact compilation.
// mix.coffee(src, output); <-- Identical to mix.js(), but registers CoffeeScript compilation.
// mix.ts(src, output); <-- TypeScript support. Requires tsconfig.json to exist in the same folder as webpack.mix.js
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.less(src, output);
// mix.stylus(src, output);
// mix.postCss(src, output, [require('postcss-some-plugin')()]);
// mix.browserSync('my-site.test');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.babelConfig({}); <-- Merge extra Babel configuration (plugins, etc.) with Mix's default.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.when(condition, function (mix) {}) <-- Call function if condition is true.
// mix.override(function (webpackConfig) {}) <-- Will be triggered once the webpack config object has been fully generated by Mix.
// mix.dump(); <-- Dump the generated webpack config object to the console.
// mix.extend(name, handler) <-- Extend Mix's API with your own components.
// mix.options({
//   extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
//   globalVueStyles: file, // Variables file to be imported in every component.
//   processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
//   purifyCss: false, // Remove unused CSS selectors.
//   terser: {}, // Terser-specific options. https://github.com/webpack-contrib/terser-webpack-plugin#options
//   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });