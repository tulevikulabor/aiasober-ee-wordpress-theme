$(function () {

    var language = $('html').attr('lang');
    console.log(language);

    var translations = [];

    translations['et'] = {
        "Luban asendada tingimusel:": "Luban asendada tingimusel:",
        "Ei soovi asendamist": "Ei soovi asendamist"
    };

    translations['en-US'] = {
        "Luban asendada tingimusel:": "I agree to the substitution on condition that:",
        "Ei soovi asendamist": "No substitution"
    };

    translations['ru-RU'] = {
        "Luban asendada tingimusel:": "Я согласен на замену при условии:",
        "Ei soovi asendamist": "Я согласен с заменой"
    };

    console.log(translations[language]);

    $('#additional_wooccm0 option').each(function () {
        var translation = translations[language];
        var val = $(this).val();
        if (translation[val]) {
            console.log(translation[val]);
            $(this).val(translation[val]);
            $(this).text(translation[val]);
        }
    });

    var newsletter_translations = {
        'et': 'Soovin liituda kampaaniateadete listiga ja saan 2-3 pakkumist kuus&nbsp;<span class="optional">(valikuline)</span>',
        'en-US': 'I want to join the list of campaign messages and receive 2-3 offers per month&nbsp;<span class="optional">(optional)</span>',
        'ru-RU': 'Я хочу присоединиться к списку сообщений компании получать 2-3 предложения месяц&nbsp;<span class="optional">(необязательно)</span>',
    };

    var mailpoet_woocommerce_checkout_optin_checkbox = '<input type="checkbox" class="input-checkbox" name="mailpoet_woocommerce_checkout_optin" id="mailpoet_woocommerce_checkout_optin" value="1">';

    $('label[data-automation-id="woo-commerce-subscription-opt-in"]').html(mailpoet_woocommerce_checkout_optin_checkbox + newsletter_translations[language]);

    $('body').on('updated_checkout', function () {
        console.log('Checkout was updated');
        $('label[data-automation-id="woo-commerce-subscription-opt-in"]').html(mailpoet_woocommerce_checkout_optin_checkbox + newsletter_translations[language]);
        console.log(newsletter_translations[language]);
    });

    $('#billing_state').val("").change();

    //$('#billing_state').val(''); // Select the option with a value of '1'
    //$('#billing_state').trigger('change'); // Notify any JS components that the value changed

});