/*----------------------------------------------
Bootstrap: Carousel
-----------------------------------------------*/

$(function () {
    $('.carousel').carousel(
        /*{
                interval: 5000,
                keyboard: true,
                pause: 'hover',
                ride: false,
                wrap: true,
                touch: true,
            }*/
    );

    $('.carousel-control-next').carousel('next');
    $('.carousel-control-prev').carousel('prev');
});

/*----------------------------------------------
Bootstrap: Collapse
-----------------------------------------------*/

/* $(function () {
    $('.collapse').collapse({
        toggle: false,
    });
}); */

/*----------------------------------------------
jQuery: viewPortWidth() & viewPortHeight()
-----------------------------------------------*/

var viewPortWidth = $(window).width();
var viewPortHeight = $(window).height();
console.log('Width: ' + viewPortWidth + ' - Height: ' + viewPortHeight);

$(window).resize(function () {
    viewPortWidth = $(window).width();
    viewPortHeight = $(window).height();
    console.log('Width: ' + viewPortWidth + ' - Height: ' + viewPortHeight);
});

function getViewPortWidth() {
    return viewPortWidth;
}

function getViewPortHeight() {
    return viewPortHeight;
}

/*----------------------------------------------
jQuery: FixTo
-----------------------------------------------*/

$(function () {

    // Header menu

    $('#header-menu-bar').fixTo('body', {
        zIndex: 20,
        useNativeSticky: false,
        top: ($('#wpadminbar').length ? $('#wpadminbar').height() : 0),
    });

    // Product categories

    /* $('.custom-woocommerce-products-sidebar').fixTo('.section-left', {
        mind: '#header-menu-bar',
        zIndex: 20,
        useNativeSticky: false,
        zIndex: 1,
        top: 0,
    });
    $('.custom-woocommerce-products-sidebar').fixTo('stop');

    if (getViewPortWidth() >= 768) {
        $('.custom-woocommerce-products-sidebar').fixTo('start');
    }

    $(window).resize(function () {
        if (getViewPortWidth() >= 768) {
            $('.custom-woocommerce-products-sidebar').fixTo('start');
        } else {
            $('.custom-woocommerce-products-sidebar').fixTo('stop');
        }
    }); */

    // Species

    $('#species-filter').fixTo('#species-filter-col', {
        mind: '#header-menu-bar',
        zIndex: 20,
        useNativeSticky: false,
        zIndex: 1,
        top: 30,
    });
    $('#species-filter').fixTo('stop');

    if (getViewPortWidth() >= 768) {
        $('#species-filter').fixTo('start');
    }

    $(window).resize(function () {
        if (getViewPortWidth() >= 768) {
            $('#species-filter').fixTo('start');
        } else {
            $('#species-filter').fixTo('stop');
        }
    });
});

/*----------------------------------------------
Bootstrap: Tooltip
-----------------------------------------------*/

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="tooltip"]').on('shown.bs.tooltip', function () {
        $('.tooltip').addClass('animated expand');
    });
});

/*----------------------------------------------
Main menu: Modal
-----------------------------------------------*/

/* $(function () {
    $('#main-menu-modal').modal('show');
}); */

/*----------------------------------------------
Header menu: Dropdown
-----------------------------------------------*/

$(function () {
    $(".header-menu-dropdown").mouseover(function () {
        $('.dropdown-toggle', this).addClass('dropdown-toggle-hover');
        $(this).addClass('show').attr('aria-expanded', "true");
        $(this).find('.dropdown-menu').addClass('show');
    }).mouseout(function () {
        $('.dropdown-toggle', this).removeClass('dropdown-toggle-hover');
        $(this).removeClass('show').attr('aria-expanded', "false");
        $(this).find('.dropdown-menu').removeClass('show');
    });

    $('.header-menu-dropdown .dropdown-toggle').click(function () {
        var location = $(this).attr('href');
        window.location.href = location;
        return false;
    });
});

/*----------------------------------------------
jQuery: Anchor scroll - https://stackoverflow.com/questions/7717527/smooth-scrolling-when-clicking-an-anchor-link
-----------------------------------------------*/

/* $(function () {
    var root = $('html, body');

    $('a[href^="#"]').click(function () {
        var href = $.attr(this, 'href');

        root.animate({
            scrollTop: $(href).offset().top
        }, 500, function () {
            window.location.hash = href;
        });

        return false;
    });
}); */

/* function smoothScrollingTo(target) {
    $('html,body').animate({
        scrollTop: $(target).offset().top
    }, 500);
}
$('a[href*=\\#]').on('click', function (event) {
    event.preventDefault();
    smoothScrollingTo(this.hash);
});
$(document).ready(function () {
    smoothScrollingTo(location.hash);
}); */