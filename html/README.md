# HTML

## Hamburgers

https://jonsuh.com/hamburgers/

https://codepen.io/erikterwan/pen/EVzeRP

## Dropdown submenu

https://gist.github.com/DrewDouglass/fb70b9ab6ddbfc044c963ae6d9b10d93

## Sliding menu

https://codepen.io/mkurapov/pen/WwVvZy/

## Box-shadow examples

https://getcssscan.com/css-box-shadow-examples

https://cssgenerator.org/box-shadow-css-generator.html

http://jsfiddle.net/afshinprofe/8YsAg/

https://codepen.io/greypants/pen/AiEgG

## package.json updates

Removed package:

```json
"@vetixy/circular-std": "https://github.com/Vetixy/circular-std",
```