<?php
use App\Cache;

$cache = new Cache(__DIR__ . "/../cache/");

$data = $cache->remember("test-cache", 3600, function () {
    echo "Refreshing data!" . PHP_EOL;
    return date("H:i:s").' --> 1';
});

echo "Latest cache save: $data<br />";

$data = $cache->remember("test-cache2", 60, function () {
    echo "Refreshing data2!" . PHP_EOL;
    return date("H:i:s").' --> 2';
});

echo "Latest cache save: $data<br />";

$data = $cache->remember("test-cache3", 5, function () {
    echo "Refreshing data2!" . PHP_EOL;
    return date("H:i:s").' --> 3';
});

echo "Latest cache save: $data<br />";

$data = $cache->remember("test-cache4", 2, function () {
    echo "Refreshing data2!" . PHP_EOL;
    return date("H:i:s").' --> 4';
});

echo "Latest cache save: $data<br />";

$data = $cache->remember("test-cache-original", 10, function () {
    echo "Refreshing data!" . PHP_EOL;
    sleep(2);
    return date("H:i:s").' --> original';
});

echo "Latest cache save: $data<br />";

echo '<pre>';
$cache->debugCache();
echo '</pre>';
