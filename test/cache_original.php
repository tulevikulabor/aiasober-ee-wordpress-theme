<?php

use Wruczek\PhpFileCache\PhpFileCache as Cache;

require_once __DIR__ . "/../vendor/autoload.php";

$cache = new Cache(__DIR__ . "/../cache/");

$data = $cache->refreshIfExpired("test-cache-original", function () {
    echo "Refreshing data!" . PHP_EOL;
    return date("H:i:s").' --> original';
}, 3600);

echo "Latest cache save: $data";
