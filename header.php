<?php
global $woocommerce;
//$cart = $woocommerce->cart;
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <!-- Meta -->
    <meta charset="<?php bloginfo('charset'); ?>" />
    <title><?php //echo get_bloginfo(); ?><?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Favicon -->
    <link rel="apple-touch-icon" href="<?php echo asset('img/favicon.png'); ?>">
    <link rel="icon" href="<?php echo asset('img/favicon.png'); ?>" sizes="32x32">
    <link rel="icon" href="<?php echo asset('img/favicon.png'); ?>" sizes="192x192">
    <meta name="msapplication-TileImage" content="<?php echo asset('img/favicon.png'); ?>">

    <!-- URL -->
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <base href="<?php //?>">
    
    <?php wp_head(); ?>
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo asset('vendor/bootstrap/bootstrap.min.css'); ?>">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo asset('css/app.css'); if(auth()->exists()){ echo '?'.date('YmdHis'); } else { echo '?'.date('YmdHis'); } ?>">

</head>

<body>
    <!-- Header -->
    <header>
        <div class="header-bar navbar p-0 m-0">
            <!-- Header: info-bar -->
            <div class="info-bar container-fluid">
                <div class="container p-0 pl-lg-3 pr-lg-3 pl-md-4 pr-md-4">
                    <ul class="info-bar-icons">
                        <li class="info-bar-icon-clock d-none d-lg-block"><?php echo get_field('contacts_open_times', 'options'); ?></li>
                        <li class="info-bar-icon-email"><?php echo get_field('contacts_e_mail', 'options', false); ?></li>
                        <li class="info-bar-icon-phone"><a href="tel:<?php echo get_field('contacts_phone', 'options'); ?>"><?php echo get_field('contacts_phone', 'options'); ?></a></li>
                    </ul>

                    <div class="lang-menu dropdown">
                        <a class="lang-menu-item dropdown-toggle" href="#" role="button" id="lang-menu-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="<?php echo asset('img/flags/' . language() . '.svg'); ?>" alt="<?php echo strtoupper(language()); ?>" />
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="lang-menu-dropdown">
                        <?php foreach (language_menu() as $key => $language) { ?>
                            <a class="dropdown-item" href="<?php echo $language->url; ?>"><img src="<?php echo asset('img/flags/' . strtolower($language->code) . '.svg'); ?>" alt="<?php echo $language->native_name; ?>" /><?php echo $language->native_name; ?></a>
                        <?php } ?>
                        <?php /* foreach (languages() as $key => $language) { ?>
                            <a class="dropdown-item" href="<?php echo $language->url; ?>"><img src="<?php echo asset('img/flags/' . $language->language_code . '.svg'); ?>" alt="<?php echo strtoupper($language->language_code); ?>" /><?php echo $language->translated_name; ?></a>
                        <?php } */ ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header: main-bar -->
            <div class="main-bar container">
                <!-- Header: logo -->
                <a href="<?php echo home_url(); ?>" class="navbar-brand p-0 m-0">
                    <img src="<?php echo asset('img/logo.svg'); ?>" alt="<?php echo get_bloginfo(); ?>" />
                </a>
                <?php // echo do_shortcode('[wd_asp id=1]'); ?>
                <!-- Header: search -->
                <?php /* <div class="header-search-bar ml-auto ml-md-0">
                    <form id="header-search-bar-form" class="form-inline">
                        <input type="text" class="form-control header-search-bar-input d-none d-md-block" id="search-input" placeholder="Otsi tooteid meie sortimendist...">
                        <button type="submit" class="btn header-search-bar-btn"></button>
                    </form>
                </div> */ ?>
                <?php get_search_form(); ?>
                <!-- Header: cart -->
                <?php woocommerce_mini_cart(); ?>
                <!-- Header: ajax-cart -->
                <script type="text/javascript">
                /*------------------------------------------------------------------------*/
                /* Load cart by Ajax
                /*------------------------------------------------------------------------*/

                if( jQuery( '.ajax-cart' ).length == 1 ) {
                    
                    function custom_load_woo_cart(){
                        
                        var data = {
                    		'action': 'load_woo_cart'
                    	};
                        
                        jQuery.post('<?php echo WC()->ajax_url(); ?>', data, function( response ) {
                    		jQuery( '.ajax-cart' ).html( response );
                    	});
                        console.log('custom_load_woo_cart()');
                    }
                    
                    custom_load_woo_cart();
                    
                    jQuery('body').on('wc_fragments_loaded', function(){
                        custom_load_woo_cart();
                        console.log('custom_load_woo_cart() after event "wc_fragments_loaded"');
                    });
                }
                </script>
            </div>
            <!-- Header: mobile-search-bar -->
            <?php include('searchform_mobile.php'); ?>
            <!-- Anchor: menu -->
            <a name="menu"></a>
            <!-- Header: menu-bar -->
            <div id="header-menu-bar" class="header-menu-bar container-fluid">
                <div class="container navbar pt-0 pb-0">
                    <div class="header-menu-burger nav d-flex d-lg-none pl-2">
                        <button class="hamburger" type="button" data-toggle="modal" data-target="#main-menu-modal">
                            <i></i><i></i><i></i>
                        </button>
                    </div>
                    <ul class="header-menu nav d-none d-lg-flex">
                    <?php foreach (menu('main_menu') as $menud_id => $main_menu) { ?>
                        <?php if (!$main_menu->children) { ?>
                            <li><a class="nav-link<?php echo ($main_menu->active) ? ' active' : ''; ?>" href="<?php echo $main_menu->url; ?>"><?php echo $main_menu->title; ?></a></li>
                        <?php } else { ?>
                            <li class="header-menu-dropdown">
                                <a class="dropdown-toggle nav-link<?php echo ($main_menu->active) ? ' active' : ''; ?>" href="<?php echo $main_menu->url; ?>" role="button" id="header-menu-dropdown-products" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $main_menu->title; ?></a>
                                <div class="dropdown-menu container">
                                    <ul class="row">
                                    <?php
                                    foreach ($main_menu->children as $menud_id => $menu) {
                                        if ($main_menu->classes[0] == 'species') {
                                            ?>
                                            <li class="col col-1 d-flex justify-content-center"><a href="<?php echo $menu->url; ?>" class="species-menu-link text-center rounded"><?php echo $menu->title; ?></a></li>
                                            <?php
                                        } else {
                                            $image = asset('img/categories/' . $menu->classes[0] . '.png');
                                            if (!file_exists(__DIR__ . '/html/dist/img/categories/' . $menu->classes[0] . '.png')) {
                                                $image = asset('img/categories/default.png');
                                            }
                                            ?>
                                            <li class="col col-3"><img src="<?php echo $image; ?>" alt="<?php echo $menu->title; ?>" /><a href="<?php echo $menu->url; ?>"><?php echo $menu->title; ?></a></li>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </ul>
                                </div>
                            </li>
                        <?php } ?>
                    <?php } ?>
                    </ul>
                    <div class="login-menu d-flex">
                        
                        <?php if(auth()->exists()){ ?>
                            
                            <?php $nr = 1; foreach (menu('logout_menu') as $menud_id => $menu) { ?>
                                <a class="login-menu-<?php echo ($nr == 1) ? 'login' : 'register'; ?>" href="<?php echo $menu->url; ?>">
                                    <?php if ( strpos($menu->title, '{username}') ) { ?>
                                        <span class="d-none d-md-block"><?php echo str_replace('{username}', ucfirst(auth()->user()->display_name), $menu->title); ?></span>
                                        <span class="d-md-none"><?php echo ucfirst(auth()->user()->display_name); ?></span>
                                    <?php } else { ?>
                                        <?php echo $menu->title; ?>
                                    <?php } ?>
                                </a>
                                <?php if ($nr == 1) { ?>
                                    <span class="login-menu-separator">/</span>
                                <?php } ?>
                            <?php $nr++; } ?>
                            
                        <?php } else { ?>
                            
                            <?php $nr = 1; foreach (menu('login_menu') as $menud_id => $menu) { ?>
                                <a class="login-menu-<?php echo ($nr == 1) ? 'login' : 'register'; ?>" href="<?php echo $menu->url; ?>"><?php echo $menu->title; ?></a><?php if ($nr == 1) { ?> <span class="login-menu-separator">/</span> <?php } ?>
                            <?php $nr++; } ?>
                            
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        
        <?php if (is_front_page() || is_home()) { ?>
            <!-- Header: header-image-bar -->
            <div class="header-image-bar container-fluid p-0 m-0">
                <div id="header-image-slider" class="carousel slide" data-interval="5000" data-ride="carousel">
                <?php
                    $active_slide = 0;
                    $default_slide_images = [
                        asset('img/header/header-image.jpg'),
                        asset('img/header/header-image-2.jpg'),
                        asset('img/header/header-image-3.jpg'),
                        asset('img/header/header-image.jpg'),
                        asset('img/header/header-image-2.jpg'),
                        asset('img/header/header-image-3.jpg'),
                        asset('img/header/header-image.jpg'),
                        asset('img/header/header-image-2.jpg'),
                        asset('img/header/header-image-3.jpg'),
                        asset('img/header/header-image.jpg'),
                        asset('img/header/header-image-2.jpg'),
                        asset('img/header/header-image-3.jpg'),
                    ];
                    $slides = cache()->remember('front-page-slides-'.language(), FRONT_PAGE_SLIDES_CACHE_TIME, function () {
                        $args = [
                            'post_type' => 'slide',
                            'status' => 'publish',
                            'posts_per_page' => 12,
                            'orderby' => 'menu_order',
                            'order'  => 'ASC',
                            'language' => language(),
                            'suppress_filters' => false,
                        ];
                        
                        return get_posts($args);
                    });
                    ?>
                    <ol class="carousel-indicators">
                    <?php $nr = 0; foreach ($slides as $slide) { ?>
                        <li data-target="#header-image-slider" data-slide-to="<?php echo $nr; ?>"<?php if($nr == $active_slide){ ?> class="active"<?php } ?>></li>
                    <?php $nr++; } ?>
                    </ol>
                    <div class="carousel-inner">
                    <?php
                    $nr = 0; foreach ($slides as $slide) {
                        
                        $slide_image = cache()->remember('image_slide_'. $slide->ID, 60, function () use ($slide, $default_slide_images, $nr) {
                            
                            $image = get_the_post_thumbnail_url($slide->ID);
                            $image = ($image) ? $image : $default_slide_images[$nr];
                            
                            return $image;
                        });
                        
                        $slide_meta = cache()->remember('meta_slide_'. $slide->ID, 60, function () use ($slide) {
                            
                            $meta = [
                                'button_name' => get_post_meta($slide->ID, 'button_name', true),
                                'button_link' => get_post_meta($slide->ID, 'button_link', true),
                                'slide_classes' => get_post_meta($slide->ID, 'slide_classes', true),
                            ];
                            return $meta;
                        });
                        ?>
                        <div class="carousel-item<?php if($nr == $active_slide){ ?> active<?php } ?>">
                            <img src="<?php echo $slide_image; ?>" class="w-100 <?php if($slide_meta['slide_classes']){ echo $slide_meta['slide_classes']; } elseif($nr == 2){ ?>slide-3<?php } ?>" alt="Püsilillide ettetellimine kevadeks 2021 on alanud!">
                            <div class="carousel-caption container p-0">
                                <div class="carousel-caption-content text-center text-md-left">
                                    <h1><?php echo $slide->post_title; ?></h1>
                                    <p><?php echo $slide->post_content; ?></p>
                                    <a href="<?php echo $slide_meta['button_link']; ?>" class="btn"><?php echo $slide_meta['button_name']; ?></a>
                                </div>
                            </div>
                        </div>
                    <?php $nr++; } ?>
                    </div>
                    <a class="carousel-control-prev" href="#header-image-slider" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only"><?php _e('Previous', 'aiasober'); ?></span>
                    </a>
                    <a class="carousel-control-next" href="#header-image-slider" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only"><?php _e('Next', 'aiasober'); ?></span>
                    </a>
                </div>
            </div>
        <?php } ?>
        
    </header>
    <a name="content"></a>
    <?php if(!is_front_page()) { ?>
        <div class="section-breadcrumb container d-flex justify-content-between pt-3">
            <div><a href="javascript:void(0)" class="btn btn-section-breadcrumb" onclick="backAway()" data-toggle="tooltip" data-placement="bottom" title="<?php _e('Back', 'aiasober'); /* ← */ ?>">←</a></div>
            <div><?php woocommerce_breadcrumb();?></div>
        </div>
    <?php } ?>